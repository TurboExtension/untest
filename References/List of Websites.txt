1. 从console.log说起（上） | AlloyTeam, http://www.alloyteam.com/2013/11/console-log/
2. Equality comparisons and sameness - JavaScript | MDN, https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness
3. Event - Web APIs | MDN, https://developer.mozilla.org/en-US/docs/Web/API/Event
4. Category: Global Ajax Event Handlers | The jQuery Foundation, http://api.jquery.com/category/ajax/global-ajax-event-handlers/
5. KeyboardEvent.keyCode - Web APIs | MDN, https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode
6. Assert Class - MSDN , https://msdn.microsoft.com/en-us/library/microsoft.visualstudio.testtools.unittesting.assert.aspx