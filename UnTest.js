/**
 * @file 基于FaceObject的单元测试框架
 * @author 孙嘉浩
 * @version 1.7.12
 */

// ATTENTION: UnTest 不应阻断应用运行，即使UnTest出错或异常。

console.warn('%c本框架仅允许运行在开发和测试环境，任何时候均不应在运营环境中使用。'
    + '\n%c否则，一旦引发安全和性能问题的，本框架及其开发者概不负责。'
    , 'color:#684911', 'color:red');

//FaceObject
var trigger, throttleTrigger, debounceTrigger, convert, protocol, FOAssign, _typeof, Database, Tablebase, Formbase, fastDBDefinition;

//UnTest
var JSOTest, VCTest, DataApiTest, UITest;
var JSAssert;

(function(){
    var _preserve = ['type', 'typeName', 'isInstanceOf', 'protocol', 'super'];
    var _global = this;
    /** @fileprivate 以字符串形式，返回键数组表示的对象
     * @return {string}
     */
    function ObjString(obj){
        if(obj === _global) {return '[Object window]';}
        return '[Object ['  + Object.keys(obj).join(',') + '] ]';
    }

    /** @fileprivate 获取数据类型 */
    var class2type = {} ;
    'Boolean Number String Function Array Date RegExp Object Error'.split(' ').forEach(function(e){
        class2type[ '[object ' + e + ']' ] = e.toLowerCase();
    }) ;
    _typeof = function(obj){
        if ( obj == null ){
            return String( obj );
        }
        return typeof obj === 'object' || typeof obj === 'function' ?
            class2type[ class2type.toString.call(obj) ] || 'object' :
            typeof obj;
    }

    //错误码采用十六进制编码, 断言不包含在内。
    _errMap = {
        //special: 0x001 - 0x00f
        0x001: 'Illegal Injection Detected! Or duplicated libraries that use variable "Class" are imported.',

        //Class: 0x010 - 0x019
        0x010: 'Duplicate difinition of name "{0}".',
        0x011: 'superClass must be a Class if defined.',
        0x012: 'initializer must be function.',
        0x013: 'staticObjects must be an object if defined.',
        0x014: '子类未实现此抽象方法',

        //DataBase: 0x020 - 0x02f
        0x020: 'DataNode.setValue accept Object only.',
        0x021: 'DataListNode.setValue accepts array only!',
        0x022: 'A Child of DataListNode can\'t be Data Forest!',
        0x023: 'Ambiguous definition: Child & Children can\'t exist in a same node.',
        0x024: 'Anonymous Child: Child must have "_name" if defined directly in "child":{}, or use "child":{"nameOfChildA": {}} instead.',
        0x025: 'The branch (of Definition Tree) that contains this part has illegal format: (the part itself maybe right, but the branch must contain error, you may need to trace up to the root node in the worst case.)',
        0x026: 'Failed to Render raw value: "{0}"',

        //protocol: 0x030-0x04f
        0x030: 'Property name "{0}" is preserved for the framework.',
        0x031: 'Property value must be an array of descriptors.',
        0x032: 'Descriptor "{0}" for property "{1}" is invalid in protocol.',
        0x033: 'Duplicated descriptor for property "{0}" is invalid in protocol.',
        0x034: 'Duplicated type descriptor (var or func) for property "{0}" is invalid in protocol.',
        0x035: 'A var property must have at least one get or set descriptor.',
        0x036: 'get or set descriptor in a func property is invalid in protocol.',
        0x037: '$0 for protocol.entrust must be a protocol template create by protocol().',
        0x038: 'Delegator "{0}" does not conform to protocol "{1}"',
        0x039: '$0 for protocol.publish must be a protocol instance return by protocol.entrust().',
        0x03a: 'Delegator "{0}" does not conform to protocol "{1}": lack of property "{2}"',
        0x03b: 'Entruster "{0}" don\'t support this protocol.',
        0x03c: 'Entruster "{0}" don\'t support the protocol "{1}".',
        
        //ViewController: 0x050 - 0x05f
        0x050: '未提供实例view定义，或this指针未指向实例',

        //UnTest: 0x800 - 0xfff
        //VCTest: 0x810 - 0x819
        0x810: 'Unknow VCTestMode: "{0}"',
        
        //DataApiTest: 0x81a - 0x81f
        0x81a: 'Unknow DataApiTestMode: "{0}"',

        //robot & Test: 0x820-0x82f
        0x820: 'You\'ve already hired a robot named "{0}"',
        0x821: '参数错误：此实例中不存在"{0}"方法！',
        0x822: '参数错误：此实例中不存在"test{0}"方法！'
    };

    function generateErrMsg(errCode){
        var errMsg = '[FOError ' + errCode + ']' + _errMap[errCode];
        for(var i = 1; i < arguments.length; ++i){
            errMsg = errMsg.replace('{'+(i-1)+'}', arguments[i]);
        }
        return errMsg;
    }

    var FOError = function (errCode) {
        //in case hex number coverted to decimal by uglify-js
        throw new Error(generateErrMsg.apply(this, arguments));
    };

    var FOConsole = {
        error: function (errCode) {
            console.error(generateErrMsg.apply(this, arguments));
        },
        warn: function (errCode) {
            console.warn(generateErrMsg.apply(this, arguments));
        }
    };

    /** @fileprivate 获取对象长度 */
    function ObjCounter(obj){
        var count = 0;
        for (var key in obj){
            if(obj.hasOwnProperty(key)) {count++;}
        }
        return count;
    }

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
    }

    _global.Class = function(name, superClass, initializer, staticObjects){
        if (_global.hasOwnProperty(name)){ //不可被占用
            throw new FOError(0x010 ,name);
        }
        if(superClass != undefined && (typeof superClass !== 'function' || !superClass.hasOwnProperty('super'))){
            throw new FOError(0x011);
        }
        if(initializer == undefined || typeof initializer !== 'function'){
            throw new FOError(0x012);
        }
        if(typeof staticObjects !== 'object' && staticObjects != undefined ) {
            throw new FOError(0x013);
        }

        Object.defineProperty(_global, name, {
            value: function(){
                var instance = initializer.apply(_global[name], arguments);
                Object.defineProperty(instance, 'typeName', {value: name, configurable:false, enumerable:true, writable:false});
                Object.defineProperty(instance, 'type', {value: _global[name], configurable:false, enumerable:true, writable:false});
                Object.defineProperty(instance, 'isInstanceOf',{ value: function (classObj) {
                    if(this.type === classObj){return true}
                    else if(this.hasOwnProperty('super')) { return this.super.isInstanceOf(classObj) }
                    return false
                },configurable:false, enumerable:true, writable:false});
                return instance;
            },
            configurable: false,
            enumerable: true,
            writable: true // Allow modify.
        });

        Object.defineProperty(_global[name], 'className', {
            value: name,
            configurable: false,
            enumerable: true,
            writable: false,
        });

        Object.defineProperty(_global[name], 'super', {
            value: superClass,
            configurable: false,
            enumerable: true,
            writable: false,
        });

        Object.defineProperty(_global[name], 'isSubClassOf', {
            value: function(classObj){
                if(!this.hasOwnProperty('super') || this['super'] == undefined || classObj == undefined)
                {return false;}
                else if (this.super === classObj){
                    return true;
                }
                else if (this.super.hasOwnProperty('isSubClassOf')){
                    return this.super.isSubClassOf(classObj);
                }
                else { return false; }
            },
            configurable: false,
            enumerable: true,
            writable: false,
        });

        for (var key in staticObjects){
            if (!staticObjects.hasOwnProperty(key)) {return}
            _global[name][key] = staticObjects[key];
        }
        return _global[name];
    }

    /** 该静态类型提供了对JSObject进行调试的机制和控制方法 */
    JSOTest = function(){
        /** @private */
        var _status = 'off';

        /** 公有成员区域 @public*/
        var self = {

            jsoReplacement: Class('JSObject', undefined, function (){

                var self = {

                    destroy: function () {
                        var strRepresentation = ObjString(this);
                        if(this.super != null && this.super.hasOwnProperty('destroy')){
                            this.super.destroy();
                        }        
                        for (var key in this) {
                            try {delete this[key]} catch (e){ continue; };
                        }
                        if(JSOTest.isOn && UnTest.debug.isFocused(this)) {
                            console.groupCollapsed('Object Destroyed: ' + this.typeName);
                            console.log(strRepresentation);
                            console.trace();
                            console.groupEnd();
                        }
                    },

                    extends: function (subClass) {
                        var instance_super = this;
                        Object.defineProperty(subClass, 'super', {
                            get: function () { return instance_super; },
                            enumerable: true
                        });
                        var instance = this.copy();
                        for (var tkey in this) {
                            if (!subClass.hasOwnProperty(tkey)) {//仅保留被子类重写的部分
                                delete this[tkey];
                            }
                        }
                        for (var ckey in subClass) {
                            if (_typeof(subClass[ckey]) === 'function'){
                                instance[ckey] = function (fnName) {
                                    return function(){
                                        if(JSOTest.isOn && UnTest.debug.isFocused(this)){
                                            console.groupCollapsed('instance function "'
                                                    + fnName + '" called from ' + this.typeName);
                                            console.log(ObjString(this));
                                            console.trace();
                                        }

                                        var startTime = performance.now();
                                        var result = subClass[fnName].apply(this, arguments);
                                        var endTime = performance.now();

                                        if(JSOTest.isOn && UnTest.debug.isFocused(this)){
                                            console.info(fnName + '()->' + result + ';');
                                            console.info('约耗时：' + (endTime - startTime) + '毫秒');
                                            console.groupEnd();
                                        }
                                        return result;
                                    };
                                }(ckey);
                            }
                            else{
                                Object.defineProperty(instance, ckey, Object.getOwnPropertyDescriptor(subClass, ckey));
                            }
                        }
                        if (this['protocol'] instanceof Array && subClass['protocol'] instanceof Array){
                            this.protocol.forEach(function(p){
                                if (instance.protocol.indexOf(p) < 0) instance.protocol.push(p);
                            });
                        }
                        return instance;
                    },

                    copy: function () {
                        var aCopy = {};
                        for (var key in this) {
                            if(_preserve.indexOf(key) >= 0)
                                aCopy[key] = this[key];
                            else
                                Object.defineProperty(aCopy, key, Object.getOwnPropertyDescriptor(this, key));
                        }
                        return aCopy;
                    }
                };
                return self;
            }),

            get isOn() {return _status === 'on'},

            /** 启动 JSObject 调试模式 */
            on: function(){
                _status = 'on';
            },

            /** 关闭 JSObject 调试模式 */
            off: function(){
                _status = 'off';
            }
        };

        return self;
    }();

    /** 该静态类型提供了基础性断言方法
     *  STANDARD: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness
     */
    JSAssert = function(){
        var passOutput = function(){
            console.groupCollapsed('%cAssertion Pass!', 'color:#50AE55');
            console.trace();
            console.groupEnd();
        };

        var self = {

            /** 真值断言，断定 value 为 true
             *  @param {any} value 仅当 value == true 时通过断言
             *  @param {boolean} [strict] 若为 true 则仅 value === true 时才可通过
             */
            True: function(value, strict){
                var result = strict ? value === true : value == true;

                if (result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 假值断言，断定 value 为 false
             *  @param {any} value
             *  @param {boolean} [strict] 若为 true 则仅 value === false 时才可通过
             */
            False: function(value, strict){
                var result = strict ? value === false : value == false;

                if (result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 一般等值断言，断定 value1 和 value2 的值相等
             *  @param {any} value1 要比较的第一个值
             *  @param {any} value2 要比较的第二个值
             */
            Equal: function(value1, value2){
                if (value1 == value2) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 一般不等值断言，断定 value1 和 value2 的值相等
             *  @param {any} value1 要比较的第一个值
             *  @param {any} value2 要比较的第二个值
             */
            Unequal: function(value1, value2){
                if (value1 != value2) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 严格等值断言，断定 value1 和 value2 的值和类型均相等
             *  @param {any} value1 要比较的第一个值
             *  @param {any} value2 要比较的第二个值
             */
            StrictlyEqual: function(value1, value2){
                if (value1 === value2) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 严格不等值断言，断定 value1 和 value2 的值和类型均相等
             *  @param {any} value1 要比较的第一个值
             *  @param {any} value2 要比较的第二个值
             */
            StrictlyUnequal: function(value1, value2){
                if (value1 !== value2) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 功能性等值断言，断定 value1 和 value2 具有相同的值、属性和方法（对于Object类型无效）。
             *  @param {any} value1 要比较的第一个值
             *  @param {any} value2 要比较的第二个值
             */
            FunctionallyEqual: function(value1, value2){
                var result;
                /**same as Object.is*/
                if (value1 === value2) {
                    result = value1 !== 0 || 1 / value1 === 1 / value2;
                } else {
                    result = value1 !== value1 && value2 !== value2;
                }

                if (result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 功能性不等值断言，断定 value1 和 value2 具有相同的值、属性和方法（对于Object类型无效）。
             *  @param {any} value1 要比较的第一个值
             *  @param {any} value2 要比较的第二个值
             */
            FunctionallyUnequal: function(value1, value2){
                var result;
                /**same as Object.is*/
                if (value1 === value2) {
                    result = value1 !== 0 || 1 / value1 === 1 / value2;
                } else {
                    result = value1 !== value1 && value2 !== value2;
                }

                if (!result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 集合等值断言(忽视原型链键值)，断定两集合具有相同的元素，且集合大小相等。
             *  @param {object|Array} value1 要比较的第一个集合
             *  @param {object|Array} value2 要比较的第二个集合
             */
            SetEqual: function (obj1, obj2) {
                var result = true;
                if(obj1 !== obj2) {
                    var counter_obj1 = 0;
                    for (var key1 in obj1){
                        if(!obj1.hasOwnProperty(key1)) {continue;}
                        ++counter_obj1;
                        if (obj2[key1] !== obj1[key1]) { result = false; }
                    }

                    var counter_obj2 = 0;
                    for (var key2 in obj2){
                        if(!obj1.hasOwnProperty(key2)) {continue;}
                        ++counter_obj2;
                    }
                    if (counter_obj1 !== counter_obj2) { result = false; }
                }

                if (result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 集合不等值断言(忽视原型链键值)，断定两集合具有相同的元素，且集合大小相等。
             *  @param {object|Array} value1 要比较的第一个集合
             *  @param {object|Array} value2 要比较的第二个集合
             */
            SetUnequal: function (obj1, obj2) {
                var result = true;
                if(obj1 !== obj2) {
                    var counter_obj1 = 0;
                    for (var key1 in obj1){
                        if(!obj1.hasOwnProperty(key1)) {continue;}
                        ++counter_obj1;
                        if (obj2[key1] !== obj1[key1]) { result = false; }
                    }

                    var counter_obj2 = 0;
                    for (var key2 in obj2){
                        if(!obj1.hasOwnProperty(key2)) {continue;}
                        ++counter_obj2;
                    }
                    if (counter_obj1 !== counter_obj2) { result = false; }
                }

                if (!result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 精度等值断言，断定value1 和 value2 在一定的精度下的值相等。
             *  @param {number|string} value1 要比较的第一个值(传入字符串可比较无限精度)
             *  @param {number|string} value2 要比较的第二个值(传入字符串可比较无限精度)
             *  @param {number} accuaracy 精度（整数），传入正整数表示要精确到的小数点后的位数，负整数表示小数点前
             */
            AccuracyEqual: function(value1, value2, accuaracy){
                var result;

                //去除首尾的零
                var trimZero = function (str) {
                    return str.replace(/(^[0]+)|([0]+$)/g, '');
                };

                var trimValue1 = value1.toString();
                if(trimValue1.indexOf('.') >= 0 && _typeof(accuaracy) === 'number'){
                    trimValue1 = trimValue1.substring(0, trimValue1.indexOf('.') + accuaracy + 1);
                }
                trimValue1 = trimZero(trimValue1);
                console.log(trimValue1);

                var trimValue2 = value2.toString();
                if(trimValue2.indexOf('.') >= 0 && _typeof(accuaracy) === 'number'){
                    trimValue2 = trimValue2.substring(0, trimValue2.indexOf('.') + accuaracy + 1);
                }
                trimValue2 = trimZero(trimValue2);
                console.log(trimValue2);

                result = trimValue1 === trimValue2;

                if (result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 精度不等值断言，断定value1 和 value2 在一定的精度下的值相等。
             *  @param {number|string} value1 要比较的第一个值(传入字符串可比较无限精度)
             *  @param {number|string} value2 要比较的第二个值(传入字符串可比较无限精度)
             *  @param {number} accuaracy 精度（整数），传入正整数表示要精确到的小数点后的位数，负整数表示小数点前
             */
            AccuracyUnequal: function(value1, value2, accuaracy){
                var result;

                //去除首尾的零
                var trimZero = function (str) {
                    return str.replace(/(^[0]+)|([0]+$)/g, '');
                };

                var trimValue1 = value1.toString();
                if(trimValue1.indexOf('.') >= 0 && _typeof(accuaracy) === 'number'){
                    trimValue1 = trimValue1.substring(0, trimValue1.indexOf('.') + accuaracy + 1);
                }
                trimValue1 = trimZero(trimValue1);
                console.log(trimValue1);

                var trimValue2 = value2.toString();
                if(trimValue2.indexOf('.') >= 0 && _typeof(accuaracy) === 'number'){
                    trimValue2 = trimValue2.substring(0, trimValue2.indexOf('.') + accuaracy + 1);
                }
                trimValue2 = trimZero(trimValue2);
                console.log(trimValue2);

                result = trimValue1 === trimValue2;

                if (!result) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 未知值断言，断定值为[undefined, null, NaN]之一
             *  注：这三个值不论有什么功能，通常都是程序需要抛弃的结果。
             *  @param {any} value 断定为未知的值
             */
            Unknown: function(value){
                if (value == null || isNaN(value)) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 已知值断言，断定值不是[undefined, null, NaN]之一
             *  注：这三个值不论有什么功能，通常都是程序需要抛弃的结果。
             *  @param {any} value 断定为已知的值
             */
            Known: function(value){
                if (value != null && isNaN(value) === false) { passOutput(); }
                else{ throw('Assertion Fail:'); }
            },

            /** 必败断言 */
            Fail: function(){
                throw('Assertion Fail:');
            }
        };
        return self;
    }();

    trigger = function (instance, handler) {
        return function (event) {
            handler.call(instance, $(this), event);
        };
    }

    var throttleTimer = [];
    throttleTrigger = function (instance, handler) {
        var _converted = function (event) {
            for(var i = 0; i < throttleTimer.length; ++i){
                var timer = throttleTimer[i];
                if(timer.handler === handler && timer.element === this){
                    timer.event = event;
                    return;
                }
            }
            handler.call(instance, $(this), event);
            var element = this;
            var newTimer = {
                handler: handler,
                instance: instance,
                event: null,
                element: element
            };
            throttleTimer.push(newTimer);
            var timeout = function(){
                if(newTimer.event != null){
                    newTimer.handler.call(newTimer.instance, $(newTimer.element), newTimer.event);
                    newTimer.event = null;
                    setTimeout(timeout, 250);
                }
                else{
                    throttleTimer.splice(throttleTimer.indexOf(newTimer), 1);
                    for(var key in newTimer){delete newTimer[key];}
                }
            };
            setTimeout(timeout, 250);
        };
        _converted['target'] = handler;
        return _converted;
    };

    var debounceTimer = [];
    debounceTrigger = function (instance, handler) {
        var _converted = function (event) {
            var timer = null;
            for(var i = 0; i < debounceTimer.length; ++i){
                if(debounceTimer[i].handler === handler && debounceTimer[i].element === this){
                    timer = debounceTimer[i];
                    clearTimeout(timer.timeout);
                    break;
                }
            }
            if(timer != null){
                timer.timeout = setTimeout(function () {
                    handler.call(instance, $(timer.element), event);
                    debounceTimer.splice(debounceTimer.indexOf(timer), 1);
                    for(var key in timer){delete timer[key];}
                }, 250);
            }
            else{
                var element = this;
                timer = {
                    handler: handler,
                    instance: instance,
                    element: element,
                    timeout: null
                };
                timer.timeout = setTimeout(function () {
                    handler.call(instance, $(timer.element), event);
                    debounceTimer.splice(debounceTimer.indexOf(timer), 1);
                    for(var key in timer){delete timer[key];}
                }, 250);
                debounceTimer.push(timer);
            }

        };
        _converted['target'] = handler;
        return _converted;
    };

    /** 该静态类型提供了对ViewController进行调试的机制和控制方法 */
    VCTest = function () {

        var originTrigger = {
            trigger: trigger,
            throttleTrigger: throttleTrigger,
            debounceTrigger: debounceTrigger
        };

        var _status = 'off';
        var eventHandlers = []; //防止在verbose模式重复log event

        var self = {

            get status() {return _status; },

            /** 启动或切换 ViewController 调试模式
             *  @param mode {string} 调试模式，可选['verbose'(aka vc-verbose), 'event'(aka vc-event)]
             */
            on: function(mode){
                _status = 'switching';
                if(mode === undefined) {mode = 'event';}
                switch (mode.toLowerCase()){
                    case 'verbose':
                    case 'vc-verbose':
                        trigger = this.triggerReplacement;
                        throttleTrigger = this.throttleTriggerReplacement;
                        debounceTrigger = this.debounceTriggerReplacement;
                        _status = 'verbose';
                        break;
                    case 'event':
                    case 'vc-event':
                        trigger = this.triggerReplacement;
                        throttleTrigger = this.throttleTriggerReplacement;
                        debounceTrigger = this.debounceTriggerReplacement;
                        _status = 'event';
                        break;
                    default:
                        FOConsole.error(0x810, mode);
                        _status = 'off';
                        break;
                }
            },

            /** 关闭 ViewController 调试模式 */
            off: function(){
                _status = 'switching';
                trigger = originTrigger.trigger;
                throttleTrigger = originTrigger.throttleTrigger;
                debounceTrigger = originTrigger.debounceTrigger;
                _status = 'off';
            },
            vcReplacement : Class('ViewController', JSObject, function (viewSelector) {
                var self = {

                    view: undefined,

                    destroy: function () {
                        var strRepresentation = ObjString(this);
                        if(this.super != null && this.super.hasOwnProperty('destroy')){
                            this.super.destroy();
                        }        
                        for (var key in this) {
                            try {delete this[key]} catch (e){ continue; };
                        }
                        if(JSOTest.isOn && UnTest.debug.isFocused(this)) {
                            console.groupCollapsed('ViewController Destroyed: ' + this.typeName);
                            console.log(strRepresentation);
                            console.trace();
                            console.groupEnd();
                        }
                    },

                    /** 获得该对象的一份浅拷贝。*/
                    copy: function () {
                        var aCopy = {};
                        for (var key in this) {
                            if(_preserve.indexOf(key) >= 0)
                                aCopy[key] = this[key];
                            else
                                Object.defineProperty(aCopy, key, Object.getOwnPropertyDescriptor(this, key));
                        }
                        return aCopy;
                    },

                    show: function (animated) {
                        if (this.view === undefined) {
                            FOError(0x050);
                        }
                        if (VCTest.status === 'verbose' && UnTest.debug.isFocused(this)){
                            console.groupCollapsed('View is shown by VCInstance：' + this.typeName);
                            console.log(ObjString(this));
                            console.trace();
                            console.groupEnd();
                        }
                        if (animated !== undefined && animated) {
                            this.view.fadeIn(); //this 用于调用实例方法。
                        }
                        else {
                            this.view.show();
                        }
                    },

                    hide: function (animated) {
                        if (this.view === undefined) {
                            FOError(0x050);
                        }
                        if (VCTest.status === 'verbose' && UnTest.debug.isFocused(this)){
                            console.groupCollapsed('View is hiden by VCInstance：' + this.typeName);
                            console.log(ObjString(this));
                            console.trace();
                            console.groupEnd();
                        }
                        if (animated !== undefined && animated) {
                            this.view.fadeOut();
                        }
                        else {
                            this.view.hide();
                        }
                    },

                    extends: function (subClass) {
                        //super机制(superClass的近似实现，虽然会降低实例的密封性，但目前这样方便调试)
                        var instance_super = this;
                        Object.defineProperty(subClass, 'super', {
                            get: function () { return instance_super; },
                            enumerable: true
                        });
                        //继承过程
                        var instance = this.copy();
                        for (var tkey in this) {
                            if (!subClass.hasOwnProperty(tkey)) {//仅保留被子类重写的部分
                                delete this[tkey];
                            }
                        }
                        for (var ckey in subClass) {
                            if(!subClass.hasOwnProperty(ckey)) {continue;}
                            var a = ckey;
                            if (_typeof(subClass[ckey]) === 'function'){
                                instance[ckey] = function(fnName){
                                    return function(){
                                        var isTriggerEvent = eventHandlers.indexOf(this[fnName]) >= 0 && arguments[1] !== undefined;

                                        if(VCTest.status === 'verbose' && !isTriggerEvent && UnTest.debug.isFocused(this)){
                                            console.groupCollapsed('instanceVC function "'
                                                    + fnName + '" called from ' + this.typeName);
                                            console.log(ObjString(this));
                                            console.trace();
                                        }

                                        var startTime = performance.now();
                                        var result = subClass[fnName].apply(this, arguments);
                                        var endTime = performance.now();

                                        if(VCTest.status === 'verbose' && !isTriggerEvent && UnTest.debug.isFocused(this)){
                                            console.info(fnName + '()->' + result + ';');
                                            console.info('约耗时：' + (endTime - startTime) + '毫秒');
                                            console.groupEnd();
                                        }
                                        return result;
                                    }
                                }(ckey);
                            }
                            else
                            {Object.defineProperty(instance, ckey, Object.getOwnPropertyDescriptor(subClass, ckey));}
                        }
                        if (this['protocol'] instanceof Array && subClass['protocol'] instanceof Array){
                            this.protocol.forEach(function(p){
                                if (instance.protocol.indexOf(p) < 0) instance.protocol.push(p);
                            });
                        }
                        return instance;
                    },

                    /** @interface 预设视图 */
                    preset: function () {
                        FOConsole.error(0x014);
                    }
                };

                /** @constructs */
                var _ViewController = this.super().extends(self);
                _ViewController.view = $(viewSelector);
                return _ViewController;
            }),


            triggerReplacement: function(instance, handler) {
                if(eventHandlers.indexOf(handler) < 0){
                    eventHandlers.push(handler);
                }
                return function (event) {
                    if(_status !== 'off' && UnTest.debug.isFocused(instance)){
                        console.groupCollapsed('%c$(' + event.target + ').'+ event.type+'();', 'color:#139EFF');
                        console.info('Complete event and instance info shown as below:');
                        console.info(event);
                        console.info('instance.typeName = ' + instance.typeName);
                        console.info(ObjString(instance));
                    }

                    var startTime = performance.now();
                    handler.call(instance, $(this), event);
                    var endTime = performance.now();

                    if(_status !== 'off' && UnTest.debug.isFocused(instance)) {
                        console.info('eventHandler 耗时: ' + (endTime - startTime) + '毫秒');
                        console.groupEnd();
                    }
                };
            },

            throttleTriggerReplacement: function (instance, handler) {
                var _converted = function (event) {
                    for(var i = 0; i < throttleTimer.length; ++i){
                        var timer = throttleTimer[i];
                        if(timer.handler === handler && timer.element === this){
                            timer.event = event;
                            return;
                        }
                    }
                    if(_status !== 'off' && UnTest.debug.isFocused(instance)){
                        console.groupCollapsed('%c$(' + event.target + ').'+ event.type+'();', 'color:#139EFF');
                        console.info('Complete event and instance info shown as below:');
                        console.info(event);
                        console.info('instance.typeName = ' + instance.typeName);
                        console.info(ObjString(instance));
                    }
                    var startTime = performance.now();
                    handler.call(instance, $(this), event);
                    var endTime = performance.now();
                    if(_status !== 'off' && UnTest.debug.isFocused(instance)) {
                        console.info('eventHandler 耗时: ' + (endTime - startTime) + '毫秒');
                        console.groupEnd();
                    }
                    var element = this;
                    var newTimer = {
                        handler: handler,
                        instance: instance,
                        event: null,
                        element: element
                    };
                    throttleTimer.push(newTimer);
                    var timeout = function(){
                        if(newTimer.event != null){
                            if(_status !== 'off' && UnTest.debug.isFocused(instance)){
                                console.groupCollapsed('%c$(' + event.target + ').'+ event.type+'();', 'color:#139EFF');
                                console.info('Complete event and instance info shown as below:');
                                console.info(event);
                                console.info('instance.typeName = ' + instance.typeName);
                                console.info(ObjString(instance));
                            }
                            var startTime = performance.now();
                            newTimer.handler.call(newTimer.instance, $(newTimer.element), newTimer.event);
                            var endTime = performance.now();
                            if(_status !== 'off' && UnTest.debug.isFocused(instance)) {
                                console.info('eventHandler 耗时: ' + (endTime - startTime) + '毫秒');
                                console.groupEnd();
                            }
                            newTimer.event = null;
                            setTimeout(timeout, 250);
                        }
                        else{
                            throttleTimer.splice(throttleTimer.indexOf(newTimer), 1);
                            for(var key in newTimer){delete newTimer[key];}
                        }
                    };
                    setTimeout(timeout, 250);
                };
                _converted['target'] = handler;
                return _converted;
            },

            debounceTriggerReplacement: function (instance, handler) {
                var _converted = function (event) {
                    var timer = null;
                    for(var i = 0; i < debounceTimer.length; ++i){
                        if(debounceTimer[i].handler === handler && debounceTimer[i].element === this){
                            timer = debounceTimer[i];
                            clearTimeout(timer.timeout);
                            break;
                        }
                    }
                    if(timer != null){
                        timer.timeout = setTimeout(function () {
                            if(_status !== 'off' && UnTest.debug.isFocused(instance)){
                                console.groupCollapsed('%c$(' + event.target + ').'+ event.type+'();', 'color:#139EFF');
                                console.info('Complete event and instance info shown as below:');
                                console.info(event);
                                console.info('instance.typeName = ' + instance.typeName);
                                console.info(ObjString(instance));
                            }

                            var startTime = performance.now();
                            handler.call(instance, $(this), event);
                            var endTime = performance.now();

                            if(_status !== 'off' && UnTest.debug.isFocused(instance)) {
                                console.info('eventHandler 耗时: ' + (endTime - startTime) + '毫秒');
                                console.groupEnd();
                            }

                            debounceTimer.splice(debounceTimer.indexOf(timer), 1);
                            for(var key in timer){delete timer[key];}
                        }, 250);
                    }
                    else{
                        var element = this;
                        timer = {
                            handler: handler,
                            instance: instance,
                            element: element,
                            timeout: null
                        };
                        timer.timeout = setTimeout(function () {
                            if(_status !== 'off' && UnTest.debug.isFocused(instance)){
                                console.groupCollapsed('%c$(' + event.target + ').'+ event.type+'();', 'color:#139EFF');
                                console.info('Complete event and instance info shown as below:');
                                console.info(event);
                                console.info('instance.typeName = ' + instance.typeName);
                                console.info(ObjString(instance));
                            }

                            var startTime = performance.now();
                            handler.call(instance, $(this), event);
                            var endTime = performance.now();

                            if(_status !== 'off' && UnTest.debug.isFocused(instance)) {
                                console.info('eventHandler 耗时: ' + (endTime - startTime) + '毫秒');
                                console.groupEnd();
                            }

                            debounceTimer.splice(debounceTimer.indexOf(timer), 1);
                            for(var key in timer){delete timer[key];}
                        }, 250);
                        debounceTimer.push(timer);
                    }

                };
                _converted['target'] = handler;
                return _converted;
            }
        };
        return self;
    }();

    /** 该静态类型提供了对DataApi进行调试的机制和控制方法 */
    DataApiTest = function () {

        var originAjax = $.ajax;
        var _status = 'off';
        var self = {
            get status() {return _status;},

            requestSend: function(event, xhr, ajaxOptions){
                console.groupCollapsed('%cAjax request send to '+ ajaxOptions.url, 'color:#303AA6');
                console.log('xhr and ajaxOptions show as the following:');
                console.info($.extend(true, {}, xhr));
                xhr.success = $.extend([this.requestSuccess], xhr.success);
                console.info($.extend(true, {}, ajaxOptions));
                console.groupEnd();
            },

            requestError: function(event, xhr, ajaxOptions, thrownError){
                console.groupCollapsed('%cAjax request error on '+ ajaxOptions.url, 'color:#FC3F1B');
                console.log('xhr, ajaxOptions and thrownError show as the following:');
                console.info($.extend(true, {}, xhr));
                console.info($.extend(true, {}, ajaxOptions));
                console.info(thrownError);
                console.groupEnd();
            },

            ajaxReplacement: function (){
                var option = arguments.length > 1 ? arguments[1] : arguments[0];
                var originOption = $.extend(true, {}, option);
                var url = arguments.length > 1 ? arguments[0] : option.url;
                var requestSuccess = function (data, textStatus, xhr){
                    console.groupCollapsed('%cReceived respond from '+ url, 'color:#118675');
                    console.log('xhr, ajaxOptions and data show as the following:');
                    console.info($.extend(true, {}, xhr));
                    console.info($.extend(true, {}, originOption));
                    console.info($.extend(true, {}, data));
                    console.groupEnd();
                };
                option.success = $.extend([], [requestSuccess, option['success']]);
                return originAjax.apply($, arguments);
            },

            /** 启动或切换 DataApi 调试模式
             *  @param mode {string} 调试模式，可选['verbose'(aka d-verbose), 'event'(aka d-event)]
             */
            on: function(mode){
                _status = 'switching';
                switch (mode.toLowerCase()){
                    case 'verbose':
                    case 'd-verbose':
                        $.ajax = this.ajaxReplacement;
                        $(document).ajaxSend(this.requestSend);
                        $(document).ajaxError(this.requestError);
                        _status = 'verbose';
                        break;
                    case 'event':
                    case 'd-event':
                        $.ajax = this.ajaxReplacement;
                        $(document).ajaxSend(this.requestSend);
                        $(document).ajaxError(this.requestError);
                        _status = 'event';
                        break;
                    default:
                        FOConsole.error(0x81a, mode);
                        _status = 'off';
                        break;
                }
            },

            /** 关闭 DataApi 调试模式 */
            off: function(){
                _status = 'switching';
                $.ajax = originAjax;
                $(document).off('ajaxSend', this.requestSend);
                $(document).off('ajaxError', this.requestError);
                _status = 'off';
            },

            dataApiReplacement: Class('DataApi', JSObject, function (dataObj) {
                /** @private */

                /** @public */
                var self = {

                    get super() { return _super; },

                    data: undefined,

                    download: function (responds) {
                        FOConsole.error(0x014);
                    },

                    upload: function (responds) {
                        FOConsole.error(0x014);
                    },

                    destroy: function () {
                        var strRepresentation = ObjString(this);
                        if(this.super != null && this.super.hasOwnProperty('destroy')){
                            this.super.destroy();
                        }        
                        for (var key in this) {
                            try {delete this[key]} catch (e){ continue; };
                        }
                        if(JSOTest.isOn && UnTest.debug.isFocused(this)) {
                            console.groupCollapsed('DataApi Destroyed: ' + this.typeName);
                            console.log(strRepresentation);
                            console.trace();
                            console.groupEnd();
                        }
                    },

                    copy: function () {
                        var aCopy = {};
                        for (var key in this) {
                            if(_preserve.indexOf(key) >= 0)
                                aCopy[key] = this[key];
                            else
                                Object.defineProperty(aCopy, key, Object.getOwnPropertyDescriptor(this, key));
                        }
                        return aCopy;
                    },

                    extends: function (subClass) {
                        //super机制(superClass的近似实现，虽然会降低实例的密封性，但目前这样方便调试)
                        var instance_super = this;
                        Object.defineProperty(subClass, 'super', {
                            get: function () { return instance_super; },
                            enumerable: true
                        });
                        //继承过程
                        var instance = this.copy();
                        for (var tkey in this) {
                            if (!subClass.hasOwnProperty(tkey)) {//仅保留被子类重写的部分
                                delete this[tkey];
                            }
                        }
                        for (var ckey in subClass) {
                            if (_typeof(subClass[ckey]) === 'function'){
                                instance[ckey] = function(fnName){
                                    return function(){
                                        if(DataApiTest.status === 'verbose' && UnTest.debug.isFocused(this)){
                                            console.groupCollapsed('instance DataApi function "'
                                                    + fnName + '" called from ' + this.typeName);
                                            console.log(ObjString(this));
                                            console.trace();
                                        }

                                        var startTime = performance.now();
                                        var result = subClass[fnName].apply(this, arguments);
                                        var endTime = performance.now();

                                        if(DataApiTest.status === 'verbose' && UnTest.debug.isFocused(this)){
                                            console.info(fnName + '()->' + result + ';');
                                            console.info('约耗时：' + (endTime - startTime) + '毫秒');
                                            console.groupEnd();
                                        }
                                        return result;
                                    }
                                }(ckey);
                            }
                            else
                            {Object.defineProperty(instance, ckey, Object.getOwnPropertyDescriptor(subClass, ckey));}
                        }
                        if (this['protocol'] instanceof Array && subClass['protocol'] instanceof Array){
                            this.protocol.forEach(function(p){
                                if (instance.protocol.indexOf(p) < 0) instance.protocol.push(p);
                            });
                        }
                        return instance;
                    },


                };

                self.data = dataObj;
                return this.super().extends(self);
            })
        };
        return self;
    }();

    /** 该静态类型提供了对用户界面进行自动化操作测试方法 */
    UITest = function(){

        var _robots = {};

        var _observers = {
            targets: [],
            observer_objs: []
        };

        var ModificationObserver = _global.MutationObserver ||
                _global.WebKitMutationObserver ||
                _global.MozMutationObserver;

        var observerOptions = {
            attributes: true, subtree: true, characterData: true,
            attributeOldValue: true, characterDataOldValue: true
        };

        function getOldObserver() {
            return {
                _target: undefined,
                observe: function (target) {
                    this._target = target;
                    $(this._target).on('DOMAttrModified DOMSubtreeModified change', domModified);
                },
                disconnect: function () {
                    $(this._target).off('DOMAttrModified DOMSubtreeModified change', domModified);
                }
            };
        }

        function domModified() {
            console.groupCollapsed('Dom Modified.');
            for (var i = 0; i < arguments.length; ++i){
                console.info(arguments[i]);
            }
            console.groupEnd();
        }


        var self = {
            /**机器人雇佣军，可以查看任意机器人的状况，委派新任务，重新设定等*/
            get robots() {return _robots},

            /** 雇佣机器人
             * @param {string} name  机器人的名字，必须是唯一的。
             * @param {object[]} taskBook  任务书，格式如下：
             * taskBook = [{
             *              task: function{$('#btn').click()}, //must wrap with function(){}
             *              delay: 0,   //单位：毫秒
             *              repeat: 1,  //若要"无限循环"，请使用Number.POSITIVE_INFINITY,
             *                  但实际上重复次数无法超过Number.Max_Value
             *              interval: 100
             *         },{...}]
             */
            hireRobot: function (name, taskBook) {
                if(_robots.hasOwnProperty(name)) {
                    FOConsole.error('0x820', name);
                    return null;
                }
                var _status = 'off';
                var delayedTaskIds = {}; // [setTimeOut()->Int]
                var repeatTaskIds = {};  // [setInterval()->Int]
                var _completedTasks = [];
                var _taskBook = $.extend(true, taskBook);

                var robot = {
                    name: name,
                    get taskBook() {return _taskBook},
                    set taskBook(newValue) {
                        if(_status === 'running' || _status === 'waiting'){
                            console.warn('🤖' + name
                                + ': Fail to assign tasks because i am running.');
                            return;
                        }
                        _taskBook = newValue;
                        _completedTasks = [];
                        _status = 'ready';
                    },

                    get status() { return _status; },

                    run: function () {
                        _status = 'waiting';
                        var instance = this;
                        var finish = function(){
                            if(ObjCounter(instance.taskBook) === _completedTasks.length
                                && ObjCounter(repeatTaskIds) === 0){
                                _status = 'finished';
                            }
                        };

                        for (var i = 0; i < this.taskBook.length; ++i){
                            (function(taskPaper, taskNo){
                                taskPaper['loop'] = 1;
                                var action = function () {
                                    if (_status ===  'pause' || _status ===  'off'){
                                        return;
                                    }
                                    if (taskPaper['repeat']  > 0 && taskPaper.loop > taskPaper['repeat'] + 1) {
                                        clearInterval(repeatTaskIds[taskNo]);
                                        delete repeatTaskIds[taskNo];
                                        finish();
                                        return;
                                    }
                                    _status = 'running';

                                    console.info('🤖 ' + name +': i\'m going to do task '
                                        + taskNo + '. (loop: ' + taskPaper.loop +')');

                                    var startTime = performance.now();
                                    try{taskPaper.task();} catch (err) {
                                        console.groupCollapsed('🤖 ' + name
                                            + ': 🚫 ERROR OCCURED ON TASK '
                                            + taskNo + '(loop: ' + taskPaper.loop +')'
                                            + '! 🚫'
                                        );
                                        console.error(err);
                                        console.groupEnd();
                                    }
                                    var endTime = performance.now();

                                    console.info('🤖 ' + name +': it takes me '
                                        + (endTime - startTime)
                                        + 'ms to finish task '
                                        + taskNo + '. (loop: ' + taskPaper.loop +')'
                                    );
                                    taskPaper.loop++;

                                    if(!repeatTaskIds.hasOwnProperty(taskNo))
                                    { _completedTasks.push(taskNo); }
                                    delete delayedTaskIds[taskNo];

                                    finish();
                                };

                                delayedTaskIds[taskNo] = setTimeout(function(){
                                    if (taskPaper['repeat'] > 0) {
                                        repeatTaskIds[taskNo] = setInterval(action, taskPaper['interval']);
                                    }
                                    action();
                                }, taskPaper['delay']);
                            })(this.taskBook[i], i);

                        }
                    },
                    
                    pause: function () {
                        if(_status !== 'running' && _status !== 'waiting'){
                            console.warn('🤖' + name
                                + ': Command "pause" is invalid because i am not running.');
                            return;
                        }
                        _status = 'pause';
                        for (var key in repeatTaskIds) {clearInterval(repeatTaskIds[key]);}
                        for (var key1 in delayedTaskIds) {clearTimeout(delayedTaskIds[key1]);}
                        repeatTaskIds = {};
                        delayedTaskIds = {};
                    },

                    resume: function () {
                        if(_status !== 'pause') {
                            console.warn('🤖 ' + name
                                + ': Command "resume" is invalid because current status isn\'t "pause".');
                            return;
                        }

                        _status = 'waiting';
                        var instance = this;
                        var finish = function(){
                            if(ObjCounter(instance.taskBook) === _completedTasks.length
                                && ObjCounter(repeatTaskIds) === 0){
                                _status = 'finished';
                            }
                        };
                        for (var i = 0; i < this.taskBook.length; ++i){
                            if(_completedTasks.indexOf(i) >= 0) {continue;}
                            (function (taskPaper, taskNo) {
                                if(!(taskPaper['loop'] > 1)) {taskPaper['loop'] = 1;}
                                var action = function () {
                                    if (_status ===  'pause' || _status ===  'off'){
                                        return;
                                    }

                                    if (taskPaper['repeat']  > 0 && taskPaper.loop >= taskPaper['repeat'] + 1) {
                                        clearInterval(repeatTaskIds[taskNo]);
                                        delete repeatTaskIds[taskNo];
                                        finish();
                                        return;
                                    }
                                    _status = 'running';

                                    console.info('🤖 ' + name +': i\'m going to do task '
                                        + taskNo + '. (loop: ' + taskPaper.loop +')');

                                    var startTime = performance.now();
                                    try{taskPaper.task();} catch (err) {
                                        console.groupCollapsed('🤖 ' + name
                                            + ': 🚫 ERROR OCCURED ON TASK '
                                            + taskNo + '(loop: ' + taskPaper.loop +')'
                                            + '! 🚫'
                                        );
                                        console.error(err);
                                        console.groupEnd();
                                    }
                                    var endTime = performance.now();

                                    console.info('🤖 ' + name +': it takes me '
                                        + (endTime - startTime)
                                        + 'ms to finish task '
                                        + taskNo + '. (loop: ' + taskPaper.loop +')'
                                    );
                                    taskPaper.loop++;

                                    if(!repeatTaskIds.hasOwnProperty(taskNo))
                                    { _completedTasks.push(taskNo); }
                                    delete delayedTaskIds[taskNo];

                                    finish();
                                };

                                delayedTaskIds[taskNo] = setTimeout(function(){
                                    if (taskPaper['repeat'] > 0) {
                                        repeatTaskIds[taskNo] = setInterval(action, taskPaper['interval']);
                                    }
                                    action();
                                }, taskPaper['delay']);
                            })(this.taskBook[i], i);

                        }
                    },

                    stop: function () {
                        _status = 'off';
                        for (var key in repeatTaskIds) {clearInterval(repeatTaskIds[key]);}
                        for (var key1 in delayedTaskIds) {clearTimeout(delayedTaskIds[key1]);}
                        repeatTaskIds = {};
                        delayedTaskIds = {};
                        _completedTasks = [];
                    }
                };
                _robots[name] = (robot);
                return robot;
            },

            fireRobot: function (name) {
                _robots[name].stop();
                delete _robots[name];
            },

            /** 元素观察器 */
            observe: function (target) {
                if(ModificationObserver != undefined){
                    $(target).on('change', domModified);
                    for (var i = 0; i < $(target).length; ++i){
                        if(_observers.targets.indexOf($(target)[i]) >= 0) {continue;}
                        var mObserver = new ModificationObserver(domModified);
                        _observers.targets.push($(target)[i]);
                        _observers.observer_objs.push(mObserver);
                        mObserver.observe($(target)[i], observerOptions);
                    }
                }
                else{
                    if(_observers.targets.indexOf($(target)) >= 0) {
                        //允许新增元素，保持与mobserver一致的行为
                        $(target).on('DOMAttrModified DOMSubtreeModified change', domModified);
                        return;
                    }
                    var oldObserver = getOldObserver();
                    oldObserver.observe(target);
                    _observers.targets.push($(target));
                    _observers.observer_objs.push(oldObserver);
                }
            },

            disconnect: function (target) {
                if(ModificationObserver != undefined){
                    $(target).off('change', domModified);
                    for (var i = 0; i < $(target).length; ++i){
                        var index = _observers.targets.indexOf($(target)[i]);
                        if(index < 0) {continue;}
                        _observers.targets.splice(index, 1);
                        var mObserver = _observers.observer_objs.splice(index, 1)[0];
                        mObserver.disconnect();
                    }
                }
                else{
                    var oindex = _observers.targets.indexOf($(target));
                    if(oindex < 0) {return;}
                    _observers.targets.splice(oindex, 1);

                    var oldObserver = _observers.observer_objs.splice(oindex, 1)[0];
                    oldObserver.disconnect();
                }
            },

            inspectZ: function(scale){
                var depth = $(_global).width() * 2;
                if(scale == undefined){
                    $('body').css('transform', 'perspective(' + depth +'px) rotateY(45deg)');
                }
                else{
                    $('body').css('transform', 'perspective(' + depth +'px) rotateY(45deg) scale('+scale+')');
                }

                function lift (childElem){
                    var base = 10;

                    for (var i = 0; i < childElem.length; ++i){
                        var child = $(childElem[i]);
                        var z = parseInt(child.css('z-index'));
                        if(isNaN(z)){ z = 0;}
                        var dist = z * 10 + base;
                        child.css('transform', 'translateX(' + dist + 'px) translateZ(' + dist + 'px)');
                        base += 2;
                    }
                    if(childElem.find('*').length > 0) {lift(childElem.find('*'));}
                }
                lift($('body').find('*'));
            }
        };

        return self;
    }();

    Class('JSObjectOrigin', undefined, function()  {
        /** @private 私有成员区域 */
        //Note: this 指向类型
        //Note: 私有成员函数通过self访问实例

        //Note: 访问父类：this.super

        /** @public 公有成员区域 */
        var self = { //Note: this 指向实例
            //Note: super是undefined时，访问super理应出错。

            /** 销毁对象实例，使其无法再被使用。*/
            destroy: function () { //Note:成员函数内访问实例的super方法：this.super.method();
                if(this.super != null && this.super.hasOwnProperty('destroy')){
                    this.super.destroy();
                }
                for (var key in this) { //不会递归删除，因为类内保存的往往是对象的引用，一般不应因本实例销毁而销毁
                    try {delete this[key]} catch (e){ continue; };
                }
            },

            /** (@instance) 继承的实现方法
             * @param {string} subClass 子类的self对象
             */
            extends: function (subClass) {
                //实例super机制
                var instance_super = this;
                Object.defineProperty(subClass, 'super', {
                    get: function () { return instance_super; },
                    enumerable: true
                });
                //继承过程
                var instance = this.copy();
                for (var tkey in this) {
                    if (!subClass.hasOwnProperty(tkey) && _preserve.indexOf(tkey) < 0) {//仅保留被子类重写的部分
                        delete this[tkey];
                    }
                }
                FOAssign(instance, subClass);
                if (this['protocol'] instanceof Array && subClass['protocol'] instanceof Array){
                    this.protocol.forEach(function(p){
                        if (instance.protocol.indexOf(p) < 0) instance.protocol.push(p);
                    });
                }
                return instance;
            },

            /** 获得该对象的一份浅拷贝。*/
            copy: function () {
                var aCopy = {};
                for (var key in this) {
                    if(_preserve.indexOf(key) >= 0)
                        aCopy[key] = this[key];
                    else
                        Object.defineProperty(aCopy, key, Object.getOwnPropertyDescriptor(this, key));
                }
                return aCopy;
            }
        };

        /** @constructs 构造函数体区域 */

        return self;
    });

    /** 该类型为单元测试的模板和基类 */
    Class('UnTest', JSObjectOrigin, function(){

        var self = {
            /** @interface
             *  测试初始化方法，该方法在本类中所有以test为开头命名的方法执行「前」被执行。
             *  此处应该编写初始化环境的代码。
             *  请注意：相关环境变量必须在整个类内可访问。
             */
            initTest: function () {
                /** @example
                 *  you can do as the following here:
                 *  1. reset the UI
                 *  2. reset the global variables
                 *  3. set up the simulate environment.
                 */
            },

            /** @interface
             *  测试方法，凡以test开头的方法均为待测试的用例
             *  此处编写启动业务流程执行的代码，例如用以模拟用户操作的代码。
             */
            testCase: function(){

            },

            /** @interface
             *  担保测试方法，该方法在本类中所有以test为开头命名的方法执行「后」被执行。
             *  此处用于测试那些必须在任何业务流程和用例结束后，都要保证正确的代码。
             *  请注意：请使用 JSAssert 或 throw() 抛出所有错误的结果。
             */
            gaurdTest: function () {
                /** @example
                 *  you can do as the following here:
                 *  1. 检查每一个 testXXX() 方法是否得到正确结果
                 *  2. 检查独立的变量或独立的模块是否能够不受干扰
                 *  3. 总之无论如何都不能出错的代码。
                 */
            },

            /**
             *  @example 你可以编写任意多个以 test、initTest 和 gaurdTest 开头的方法
             *  以便于你能够测试多个用例，和分块编写initTest和gaurdTest测试代码。
             *  多个initTest（或gaurdTest）会按照各自在类中出现的顺序执行。
             */

            /**
             *  运行测试。
             *  @param {string|string[]} [methodName] 以test开头的方法名
             *  若不提供用例名，将执行全部的测试
             */
            run: function(methodName){
                //检查参数
                if(_typeof(methodName) === 'string'){
                    if( (!this.hasOwnProperty(methodName))
                        || (_typeof(this[methodName]) !== 'function') ){
                        FOConsole.error(0x821, methodName);
                        return;
                    }
                    methodName = [methodName];
                }
                else if(_typeof(methodName) === 'Array'){
                    for (var i = 0; i < methodName.length; ++i){
                        if( (!this.hasOwnProperty(methodName[i]))
                            || (_typeof(this[methodName[i]]) !== 'function') ){
                            FOConsole.error(0x822, methodName[i]);
                            return;
                        }
                        methodName[i] = methodName[i];
                    }
                }
                else{
                    methodName = [];
                    for (var key in this){
                        if(!this.hasOwnProperty(key)){ continue; }
                        if(key.indexOf('test') === 0 && _typeof(this[key]) === 'function'){
                            methodName.push(key);
                        }
                    }
                }

                //找齐初始化和守护方法
                var initializers = [];
                var guards = [];
                for(var method in this){
                    if(!this.hasOwnProperty(method)){ continue; }
                    if(_typeof(this[method]) !== 'function'){ continue; }
                    if(method.indexOf('initTest') === 0){ initializers.push(method); }
                    if(method.indexOf('gaurdTest') === 0){ guards.push(method); }
                }

                //执行测试
                for(var k = 0; k < methodName.length; ++k){
                    console.log('------');
                    console.log(methodName[k] + ': initializing.');
                    for (var n = 0; n < initializers.length; ++n){
                        this[initializers[n]]();
                    }
                    console.log(methodName[k] + ': running test.');

                    var startTime = performance.now();
                    this[methodName[k]]();
                    var endTime = performance.now();

                    console.log('%c' + methodName[k] + ': pass within '
                        + (endTime - startTime) + 'ms.',
                        'color:#118675'
                    );
                    console.log(methodName[k] + ': Guardians working.');
                    for (var m = 0; m < guards.length; ++m){
                        this[guards[m]]();
                    }
                    console.log(methodName[k] + ': Guardians leave.');
                }
                console.log('Test Finished!');
            },


            /**
             *  运行速度测试。
             *  @param {string} methodName 要进行速度测试的方法名（如speedTest）, 限一个
             *  @param {number} baseline 基准线（单位：毫秒, 必须大于0，初次运行建议设置1000ms）
             *  @param {number} [max_stddev] 幅度的标准差的最大值（单位：%，默认值10%）
             *  若不提供用例名，将执行全部的测试
             *  规则如下：
             *  1. 平均运行速度不能比baseline慢baseline的10%
             *  2. （（每轮用时-平均用时）／ 平均用时) * 100 < 40%
             *  3. 第二条的值的标准差不能超过max_stddev
             */
            runfast: function(methodName, baseline, max_stddev) {
                //检查参数
                if ((!this.hasOwnProperty(methodName))
                    || (_typeof(this[methodName]) !== 'function')) {
                    console.error(0x821, methodName);
                    return;
                }
                if(max_stddev == undefined) {max_stddev = 10;}

                //找齐初始化和守护方法
                var initializers = [];
                var guards = [];
                for (var method in this) {
                    if (!this.hasOwnProperty(method)) {
                        continue;
                    }
                    if (_typeof(this[method]) !== 'function') {
                        continue;
                    }
                    if (method.indexOf('initTest') === 0) {
                        initializers.push(method);
                    }
                    if (method.indexOf('gaurdTest') === 0) {
                        guards.push(method);
                    }
                }

                //执行测试
                var durations = [];
                for (var k = 0; k < 10; ++k) {
                    for (var n = 0; n < initializers.length; ++n) {
                        this[initializers[n]]();
                    }

                    var startTime = performance.now();
                    this[methodName]();
                    var endTime = performance.now();
                    durations.push((endTime - startTime));

                    for (var m = 0; m < guards.length; ++m) {
                        this[guards[m]]();
                    }
                }
                console.log('Speed Test Finished!');


                //计算平均用时
                var total = durations.reduce(function (sum, value) {
                    return sum + value;
                }, 0);
                var avg = total / 10;

                var rule1_result = ((avg - baseline) / baseline) * 100;
                var rule1_fail = rule1_result > 10;

                //计算每轮用时与平均值偏差幅度
                var ranges = [];
                var rule2_fail = false;
                var rule2_result = NaN;
                for (var r = 0; r < durations.length; ++r) {
                    ranges[r] = ((durations[r] - avg) / avg) * 100;
                    if (rule2_fail === false && Math.abs(ranges[r]) > 40) {
                        rule2_result = r;
                        rule2_fail = true;
                    }
                }
                var r_avg = ranges.reduce(function (sum, value) {
                    return sum + value;
                }, 0);

                //计算幅度标准差
                var vtotal = ranges.reduce(function (sum, value) {
                    return sum + (value - r_avg) * (value - r_avg);
                }, 0);
                var stddev = Math.sqrt(vtotal / 10);

                var rule3_fail = stddev > max_stddev;

                var fail_reason = undefined;
                if (rule1_fail) {
                    fail_reason = '平均用时比基准线慢' + rule1_result + '% (仅允许小于等于10%)';
                }
                else if (rule2_fail) {
                    fail_reason = '第' + rule2_result + '轮用时与平均值偏差高达'
                        + ranges[rule2_result] + '% (仅允许在[-40%, 40%]范围内)';
                }
                else if (rule3_fail) {
                    fail_reason = '每轮用时幅度的标准差为' + stddev + '%，大于' + max_stddev + '%(max_stddev)';
                }

                var consoleColor;
                var groupTitle = '';
                if (fail_reason === undefined) {
                    if (rule1_result <= 0) {
                        consoleColor = '#118675';
                        groupTitle = '平均用时' + avg + 'ms，比基准线快' + Math.abs(rule1_result) + '%(±' + stddev + '%)';
                    }
                    else {
                        consoleColor = '#FABC40'
                        groupTitle = '平均用时' + avg + 'ms，比基准线慢' + rule1_result + '%(±' + stddev + '%)';
                    }
                }
                else {
                    consoleColor = '#FC3F1B';
                    groupTitle = fail_reason;
                }

                console.groupCollapsed('%c' + groupTitle, 'color:' + consoleColor);
                console.info('报告：比基准线' + (rule1_result <= 0 ? '快' : '慢')
                    + Math.abs(rule1_result) + '%(±' + stddev + '%)');
                console.info('平均用时：' + avg + 'ms');
                console.info('基准线：' + baseline + 'ms');
                console.info('最大幅度标准差：' + max_stddev + '%');
                console.info('每轮测试数据如下：');
                for (var c = 0; c < durations.length; ++c) {
                    console.info('第' + c + '轮：' + durations[c] +'ms（' + ranges[c] +'%）');
                }
                console.groupEnd();
            },
        };

        return this.super().extends(self);
    },  // 静态属性
        {debug: function(){

        var focusList = [];
        var self = {
            /** 开启模式
             *
             *  @param {string} mode 模式
             *  可选：'verbose'|'event'|'vc-verbose'|'vc-event'|'d-verbose'|'d-event'
             */
            on: function (mode) {
                switch (mode.toLowerCase()){
                    case 'verbose':
                        JSOTest.on();
                        DataApiTest.on('event');
                        break;
                    case 'event':
                        DataApiTest.on(mode);
                        VCTest.on(mode);
                        break;
                    case 'vc-verbose':
                    case 'vc-event':
                        VCTest.on(mode);
                        break;
                    case 'd-verbose':
                    case 'd-event':
                        DataApiTest.on(mode);
                        break;
                    default:
                        break;
                }
            },

            /** 聚焦控制台输出
             *  @param {object|object[]} instance 要聚焦的实例
             *  (聚焦模式对于 d-event 的输出无效)
             */
            focus: function(instance){
                if(_typeof(instance) === 'array'){
                    for(var i = 0; i < instance.length; ++i){
                        if(focusList.indexOf(instance[i]) < 0){
                            focusList.push(instance[i])
                        }
                    }
                }
                else{
                    if(focusList.indexOf(instance) < 0){
                        focusList.push(instance)
                    }
                }
            },

            /**去除焦点*/
            blur: function(instance){
                if(_typeof(instance) === 'array'){
                    for(var i = 0; i < instance.length; ++i){
                        var index = focusList.indexOf(instance[i]);
                        if(index > 0){ focusList.splice(index, 1); }
                    }
                }
                else{
                    var index2 = focusList.indexOf(instance);
                    if(index2 > 0){ focusList.splice(index2, 1); }
                }
            },

            isFocused: function (instance) {
                if(focusList.length === 0) { return true;}
                if(focusList.indexOf(instance) >= 0) { return true;}
                return false;
            },

            /** 关闭调试模式 */
            off:function (mode) {
                if(mode == undefined) {mode = '';}
                switch (mode.toLowerCase()){
                    case 'verbose':
                        JSOTest.off();
                        DataApiTest.off();
                        break;
                    case 'event':
                        DataApiTest.off();
                        VCTest.off();
                        break;
                    case 'vc-verbose':
                    case 'vc-event':
                        VCTest.off();
                        break;
                    case 'd-verbose':
                    case 'd-event':
                        DataApiTest.off();
                        break;
                    default:
                        JSOTest.off();
                        VCTest.off();
                        DataApiTest.off();
                        break;
                }
            },
        };

        return self;
    }()});

    convert = function(type, options) {
        switch (type) {
            case 'string':
                return function (value) { return value == undefined ? '' : value.toString(); };
            case 'int':
                return function (value) { return parseInt(value); };
            case 'float':
                return function (value) { return parseFloat(value); };
            case 'date':
                return function (value) { return new Date(value); };
            case 'shortDate':/**@non-standard support for 2018-05-25T18:15:00 to 05-25 18:15*/
                return function (value) {return value == null ? '' : value.substring(5, 16).replace('T', ' '); };
            case 'option':
                return function (value) { return options[value]; };
            default:
                return function (value) { return value; };
        }
    }

    //polyfill for getOwnPropertyDescriptors
    if(!Object.hasOwnProperty('getOwnPropertyDescriptors')){
        Object.defineProperty(Object, 'getOwnPropertyDescriptors', {
            writable: true,
            configurable: true,
            value: function (obj) {
                var descriptors = Object.keys(obj).reduce(function(descriptors, key) {
                    descriptors[key] = Object.getOwnPropertyDescriptor(obj, key);
                    return descriptors;
                }, {});
                return descriptors;
            }
        });
    }

    /**将源对象的全部属性拷贝到目标对象中(浅拷贝)
     * @param {object} target 目标对象
     * @param {object} source 源对象
     * @return {object} 目标对象
     */
    FOAssign = function (target, source) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
        return target;
    };

    /**REGION(DATABASE)*/
    var _cellProto = {
        'boolean': Boolean,
        'number': Number,
        'string': String,
        'array':Array,
        'date':Date,
        // 'regExp': RegExp,
        // 'object': Object,
        // 'error': Error,
        // 'function':Function,
    };
    /** @Warning cut the code below before prepack, and paste it back in .pack.js and here*/
    (function(){
        for(var t in _cellProto){
            var proto = _cellProto[t].prototype;
            var pnames = Object.getOwnPropertyNames(proto);
            _cellProto[t] = {};
            for (var p = 0; p < pnames.length; ++p) {
                Object.defineProperty(_cellProto[t], pnames[p], {
                    get: function (pname) {
                        return function () {
                            return this.raw == null ? null : this.raw[pname]
                        }
                    }(pnames[p])
                })
            }
        }
    }).call();
    /** @Warning cut the code above before prepack, and paste it back in .pack.js and here*/

    Class('DataCell', JSObject, function(initType){
        var _raw = undefined;
        var _default = undefined;
        var _formatted = undefined;
        var self = {
            attr: {},
            get raw(){
                return _raw;
            },
            reset: function () {
                this.setValue(_default);
            },

            destroy: function (shouldDestroyElem) {
                for(var child in this){
                    if(_typeof(this[child]) === 'object' && this[child].hasOwnProperty('setValue')){
                        if(this[child].hasOwnProperty('destroy')) {this[child].destroy(shouldDestroyElem);}
                    }
                }

                if(shouldDestroyElem) {$(this.element).remove();}
                if(this.super != null && this.super.hasOwnProperty('destroy')){
                    this.super.destroy();
                }
                for (var key in this) {
                    try {delete this[key];} catch (e){ continue; }
                }
            },

            render: function () {
                var value = this.getValue();
                if(!this.hasOwnProperty('element')){
                    var prefix = this['prefix'];
                    if(prefix == null){
                        if(/[A-Z]/.test(this._name.substring(0,1))){
                            prefix = '#';
                        }
                        else{
                            prefix = '.';
                        }
                    }
                    this['element'] = $(prefix + this._name + (this.hasOwnProperty('suffix') ? this.suffix : ''));
                }
                if(_typeof(this.element) === 'string'){this.element = $(this.element);}
                if(this.element.length === 0) {return;}

                var inputTag = ['INPUT', 'SELECT', 'TEXTAREA'];
                var instance = this;
                this.element.each(function (i, el) {
                    if(el.tagName == null){return;} //不支持文本节点
                    if($(el).attr('type') === 'checkbox'){
                        $(el).prop('checked', instance.raw == true).trigger('change');
                    }
                    else if($(el).attr('type') === 'radio'){
                        if($(el).attr('name') == instance._name && $(el).val() == value){
                            $(el).prop('checked', true).trigger('change');
                        }
                    }
                    else if(inputTag.indexOf(el.tagName.toUpperCase()) >= 0){
                        if($(el).val() != value)
                        {$(el).val(value).trigger('input').trigger('change');}
                    }
                    else{
                        if($(el).text() != value || ($(el).text() === '' && value !== ''))
                        {$(el).text(value).trigger('DOMCharacterDataModified').trigger('change');}
                    }

                    var eventObj = $._data(el, 'events');
                    if(eventObj == null || !eventObj.hasOwnProperty('change')){
                        $(el).on('input change propertychange DOMCharacterDataModified', trigger(instance, instance.elementEvent));
                    }
                    else{
                        var handlerExist = false;
                        for(var h = 0; h < eventObj.change.length; ++h){
                            var handlerObj = eventObj.change[h].handler;
                            if(handlerObj.hasOwnProperty('target') && handlerObj.target === instance.elementEvent){
                                handlerExist = true;
                            }
                        }
                        if(!handlerExist){
                            $(el).on('input change propertychange DOMCharacterDataModified', trigger(instance, instance.elementEvent));
                        }
                    }
                });
                this.renderAttr();
                return this.element;
            },

            renderAttr: function(){
                for(var key in this.attr){
                    var attr = this.attr[key].call(this, this.getValue(), this.element.attr(key));
                    if(attr != null && attr.length > 0) {this.element.attr(key, attr);}
                }
            },

            getElemValue : function (elem) {
                var inputTag = ['INPUT', 'SELECT', 'TEXTAREA'];
                var elemValue;
                if(elem.attr('type') === 'checkbox'){
                    elemValue = elem.prop('checked');
                    if(_typeof(elemValue) == 'string')
                    {elemValue = elemValue === 'checked' ? true : false;}
                }
                else if(elem.attr('type') === 'radio'){
                    if(elem.attr('name') != this._name){return undefined;}
                    if(elem.val() == this.getValue()
                        && elem.prop('checked') !== true
                        && elem.prop('checked') !== 'checked'){
                        return undefined;
                    }
                    elemValue = elem.val();
                }
                else if(inputTag.indexOf(elem[0].tagName.toUpperCase())>=0) { elemValue = elem.val(); }
                else { elemValue = elem.text(); }
                return elemValue;
            },

            elementEvent: function (elem) {
                var elemValue = this.getElemValue(elem);
                if(elemValue != undefined && this.getValue() != elemValue) { this.setValue(elemValue, elem); }
            },

            setValue: function (value, fromElem) {
                var shouldSetDefault = false;
                if(_raw === undefined && _default === undefined) {shouldSetDefault = true;}
                if (this.hasOwnProperty('filter')) {
                    _raw = this.filter(value);
                }
                else {
                    _raw = value;
                }
                if(shouldSetDefault) {_default = _raw;}
                if(this.hasOwnProperty('formatter')){_formatted = this.formatter(_raw);}
                if (this.hasOwnProperty('render')) {
                    //防止循环渲染不可渲染的值
                    if(fromElem != null && this.getValue() != this.getElemValue(fromElem))
                    {FOConsole.warn(0x26, _raw);}
                    else
                    {this.render();}
                }
                this._notifier(this.getValue());
            },

            _notifier: function (value) {
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(value);
                }
                if(this.hasOwnProperty('fatherNotifier')){
                    this.fatherNotifier(value);
                }
            },

            getValue: function () {
                if (this.hasOwnProperty('formatter')) {
                    return _formatted;
                }
                else {
                    return _raw;
                }
            },

            //alias of getValue
            valueOf: function () {
                return this.getValue();
            },

            toString: function () {
                return this.getValue().toString();
            }
        };
        var _Cell = this.super().extends(self);
        if(_cellProto.hasOwnProperty(initType)){
            _Cell = Object.create(_cellProto[initType], Object.getOwnPropertyDescriptors(_Cell));
        }
        return _Cell;
    });

    Class('DataNode', JSObject, function () {
        var canNotify = true;
        var self = {
            attr:{},
            valueOf: function () {
                return this.getValue();
            },
            getRawValue: function () {
                var rawObj = {};
                for (var key in this){
                    if(!this.hasOwnProperty(key)){continue;}
                    if(_typeof(this[key]) == 'object'){
                        if(this[key].hasOwnProperty('getRawValue')) {rawObj[key] = this[key].getRawValue();}
                        else if(this[key].hasOwnProperty('raw')) {rawObj[key] = this[key].raw;}
                    }
                }
                return rawObj;
            },

            getValue: function () {
                var rawObj = {};
                for (var key in this){
                    if(!this.hasOwnProperty(key)){continue;}
                    if(_typeof(this[key]) == 'object' && this[key].hasOwnProperty('getValue')){
                        rawObj[key] = this[key].getValue();
                    }
                }
                return rawObj;
            },

            setValue: function (value) {
                if(this.hasOwnProperty('filter')) {value = this.filter(value);}
                if(_typeof(value) !== 'object'){
                    FOError(0x020);
                }

                for(var key in value){
                    if(!value.hasOwnProperty(key) || ['setValue', 'notifier', 'render'].indexOf(key) >= 0){ continue; }

                    if(this.hasOwnProperty(key)){
                        this[key] = value[key];
                    }
                    else{
                        Object.defineProperty(this, key, Object.getOwnPropertyDescriptor(value, key));
                    }
                    if(_typeof(this[key]) === 'object' && this[key].hasOwnProperty('notifier')){
                        if(!this[key].hasOwnProperty('fatherNotifier')) {
                            this[key]['fatherNotifier'] = function(father){
                                return function(){father._notifier();}
                            }(this)
                        };
                    }
                }
                if (this.hasOwnProperty('render')) {
                    this.render().trigger('change');
                }

                this._notifier(this.getValue());
            },

            updateValue: function(value){
                if(this.hasOwnProperty('filter')) {value = this.filter(value);}
                if(_typeof(value) !== 'object'){
                    FOError(0x020);
                }
                canNotify = false;
                for(var key in value){
                    if(!value.hasOwnProperty(key) || ['setValue', 'notifier', 'render'].indexOf(key) >= 0){ continue; }

                    if(this.hasOwnProperty(key)){
                        if(this[key].hasOwnProperty('raw')){
                            if(this[key].hasOwnProperty('filter')){
                                if(this[key].filter(value) !== this[key].raw) {this[key] = value[key];}
                            }
                            else {
                                if(value !== this[key].raw) {this[key] = value[key];}
                            }
                        }
                        else{this[key] = value[key];}
                    }
                    else{
                        Object.defineProperty(this, key, Object.getOwnPropertyDescriptor(value, key));
                    }
                    if(_typeof(this[key]) === 'object' && this[key].hasOwnProperty('notifier')){
                        if(!this[key].hasOwnProperty('fatherNotifier')) {
                            this[key]['fatherNotifier'] = function(father){
                                return function(){father._notifier();}
                            }(this)
                        };
                    }
                }

                if (this.hasOwnProperty('render')) {
                    try{
                        if(_typeof(this.element) == 'string' || $(this.element).length === 0){
                            this.render().trigger('change');
                        }
                    }
                    catch (e){}
                }
                canNotify = true;
                this._notifier(this.getValue());
            },

            _notifier: function (value) {
                if(!canNotify) {return;}
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(value);
                }
                if(this.hasOwnProperty('fatherNotifier')){
                    this.fatherNotifier(value);
                }
            },

            toString: function(){
                return JSON.stringify(this.getValue());
            },

            toRawString: function(){
                return JSON.stringify(this.getRawValue());
            },

            reset: function () {
                for(var child in this){
                    if(_typeof(this[child]) !== 'object' || !this[child].hasOwnProperty('reset')){ continue; }
                    this[child].reset();
                }
            },

            destroy: function (shouldDestroyElem) {
                for(var child in this){
                    if(_typeof(this[child]) === 'object' && this[child].hasOwnProperty('setValue')){
                        if(this[child].hasOwnProperty('destroy')) {this[child].destroy(shouldDestroyElem);}
                    }
                }

                if(shouldDestroyElem) {$(this.element).remove();}
                if(this.super != null && this.super.hasOwnProperty('destroy')){
                    this.super.destroy();
                }
                for (var key in this) {
                    try {delete this[key];} catch (e){ continue; }
                }
            },

            render: function () {
                if(!this.hasOwnProperty('element') && this.hasOwnProperty('_name')){
                    var prefix = this['prefix'];
                    if(prefix == null){
                        if(/[A-Z]/.test(this._name.substring(0,1))){
                            prefix = '#';
                        }
                        else{
                            prefix = '.';
                        }
                    }
                    this['element'] = $(prefix + this._name + (this.hasOwnProperty('suffix') ? this.suffix : ''));
                }
                if(_typeof(this['element']) === 'string'){this.element = $(this.element);}
                if(this['element'] == null || this['element'].length === 0) {return;}

                if(this.forceRedraw !== false){this.element.html('');}
                for(var child in this){
                    if((!this.hasOwnProperty(child)) || (_typeof(this[child])!== 'object')){continue;}
                    if(this[child].hasOwnProperty('render'))
                    {
                        var childElem = this[child].render();
                        if (childElem != null && childElem.length > 0) { this.element.append(childElem); }
                    }
                    else if(this[child].hasOwnProperty('element'))
                    {this.element.append(this[child].element);}
                }
                this.renderAttr();
                return this.element;
            },

            renderAttr: function(){
                for(var key in this.attr){
                    var attr = this.attr[key].call(this, this.element.attr(key));
                    if(attr != null && attr.length > 0) {this.element.attr(key, attr);}
                }
            }
        };
        return this.super().extends(self);
    });

    Class('DataListNode', JSObject, function (childrenModel) {
        var _children = [];
        var _childModel = {};
        var _presetMap = {
            tbody: ['<tbody>', '<tr>', '<td>'],
            ul: ['<ul>', '<li>'],
            ol: ['<ol>', '<li>'],
            select: ['<select>', '<option>'],
            select_group: ['<select>', '<optgroup>', '<option>']
        };
        var _applyPresetRender = function () {
            var elemPreset;
            for(var preset in DataListNode['render']){
                if(this.render === DataListNode['render'][preset]){
                    elemPreset = _presetMap[preset];
                }
            }
            if(elemPreset == null) {return;}

            if(!this.hasOwnProperty('element') && this.hasOwnProperty('_name')){
                var prefix = this['prefix'];
                if(prefix == null){
                    if(/[A-Z]/.test(this._name.substring(0,1))){
                        prefix = '#';
                    }
                    else{
                        prefix = '.';
                    }
                }
                this['element'] = $(prefix + this._name + (this.hasOwnProperty('suffix') ? this.suffix : ''));
            }
            if(this['element'] == null || this['element'].length === 0) {
                this.element = elemPreset[0];
            }

            var layerIterator = function(layer, childPointer){
                if(!childPointer.hasOwnProperty('element')){
                    childPointer.element = elemPreset[layer];
                }

                if(childPointer.hasOwnProperty('children')){
                    layerIterator(layer + 1, childPointer.children);
                }
                else if(childPointer.hasOwnProperty('child')){
                    for(var subLayer in childPointer.child){
                        layerIterator(layer + 1, childPointer.child[subLayer]);
                    }
                }
            };
            layerIterator(1, this.childModel);
        };
        var canNotify = true;
        var self = {
            attr: {},
            get items() {return _children;},
            set items(newValue) { this.setValue(newValue);},

            get childModel() {return _childModel;},
            set childModel(newValue) {
                _childModel = newValue;
                if(_childModel.hasOwnProperty('notifier')){
                    _childModel['fatherNotifier'] = function(father){
                        return function(){father._notifier();}
                    }(this)
                }
                _applyPresetRender.call(this);
            },

            render: function () { return DataListNode.render.default.call(this);},

            renderAttr: function(){
                for(var key in this.attr){
                    var attr = this.attr[key].call(this, this.element.attr(key));
                    if(attr != null && attr.length > 0) {this.element.attr(key, attr);}
                }
            },

            //filter: function(value, replace){_typeof(value) === 'array' && _typeof(replace) === 'boolean'}
            setValue: function (value) {
                if(this.hasOwnProperty('filter')){value = this.filter(value, true);}
                if(_typeof(value) !== 'array'){ FOError(0x021); }

                _children = [];
                for (var i = 0; i < value.length; ++i){
                    var child = Database(this.childModel);
                    if(child.hasOwnProperty('setValue')){
                        child.setValue(value[i]);
                    }
                    else if(Object.keys(child).length === 1 && child[Object.keys(child)[0]].hasOwnProperty('setValue')){
                        child.setValue(value[i]);
                    }
                    else{
                        FOError(0x022);
                    }
                    child.render();
                    _children.push(child);
                }

                if (this.hasOwnProperty('render')) { this.render().trigger('change'); }
                this._notifier(this.getValue());
            },

            //not allow custom render
            updateValue: function(value){
                if(this.hasOwnProperty('filter')){value = this.filter(value, true);}
                if(_typeof(value) !== 'array'){ FOError(0x021); }
                canNotify = false;
                for(var i = 0; (i < _children.length) && (i < value.length); ++i){
                    _children[i].updateValue(value[i]);
                }
                if(_children.length > value.length){
                    for(var k = _children.length - 1; k >= value.length; --k){
                        _children.pop().destroy(true);
                    }
                }
                else if(value.length > _children.length){
                    if(_typeof(this['element']) === 'string'){this.element = $(this.element);}
                    for(var j = _children.length; j < value.length; ++j){
                        var child = Database(this.childModel);
                        if(child.hasOwnProperty('setValue')){
                            child.setValue(value[j]);
                        }
                        else if(Object.keys(child).length === 1 && child[Object.keys(child)[0]].hasOwnProperty('setValue')){
                            child.setValue(value[j]);
                        }
                        else{
                            FOError(0x022);
                        }
                        child.render();
                        _children.push(child);

                        this.element.append(_children[j].element);
                    }
                }
                if(_typeof(this['element']) === 'string')
                {this.render().trigger('change');}
                else
                {this.element.trigger('change');}
                canNotify = true;
                this._notifier(this.getValue());
            },

            _notifier: function (value) {
                if(!canNotify) {return}
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(value);
                }
                if(this.hasOwnProperty('fatherNotifier')){
                    this.fatherNotifier(value);
                }
            },

            reset: function () {
                for(var i = 0; i < this.items.length; ++i){
                    if(this.items[i].hasOwnProperty('reset')) {this.items[i].reset();}
                }
            },

            pop: function () {
                var child = _children.pop();
                child.element.remove();
                this.element.trigger('change');
                this._notifier(this.getValue());
                return child;
            },

            push: function (value) {
                if(this.hasOwnProperty('filter')){value = this.filter([value], false)[0];}
                var child = Database(this.childModel);
                if(child.hasOwnProperty('setValue')){
                    child.setValue(value);
                }
                else if(Object.keys(child).length === 1 && child[Object.keys(child)[0]].hasOwnProperty('setValue')){
                    child.setValue(value);
                }
                else{
                    FOError(0x022);
                }
                _children.push(child);
                this.element.append(child.render());
                this.element.trigger('change');
                this._notifier(this.getValue());
            },

            shift: function () {
                var child = _children.shift();
                child.element.remove();
                this.element.trigger('change');
                this._notifier(this.getValue());
                return child;
            },

            splice: function (start, deleteCount) {
                var deleted;
                if(arguments.length > 2){
                    var argvs = [start, deleteCount];
                    for(var i = 2; i < arguments.length; ++i){
                        var child = Database(this.childModel);
                        var value;
                        if(this.hasOwnProperty('filter')) {value = this.filter([arguments[i]], false)[0];}
                        else{value = arguments[i];}

                        if(child.hasOwnProperty('setValue')){
                            child.setValue(value);
                        }
                        else if(Object.keys(child).length === 1 && child[Object.keys(child)[0]].hasOwnProperty('setValue')){
                            child.setValue(value);
                        }
                        else{
                            FOError(0x022);
                        }
                        child.render().insertBefore(_children[start].element);
                        argvs.push(child);
                    }
                    deleted = _children.splice.apply(_children, argvs);
                }
                else{
                    deleted = _children.splice.apply(arguments);
                }
                for(var k = 0; k <deleted.length; ++k){
                    deleted[k].element.remove();
                }
                this.element.trigger('change');
                this._notifier(this.getValue());
            },

            unshift: function () {
                this.splice.apply(this, [0, 0].concat(arguments));
                this.element.trigger('change');
                this._notifier(this.getValue());
            },

            sort: function () {
                if(_children.length < 2) {return;}
                var originFirstElement = _children[0].element;
                _children.sort.apply(_children, arguments);
                if(_children[0].element !== originFirstElement)
                {_children[0].element.insertBefore(originFirstElement)}
                for(var i = 1; i < _children.length; ++i){
                    _children[i].element.insertAfter(_children[i-1].element);
                }
                this.element.trigger('change');
                this._notifier(this.getValue());
            },

            reverse: function () {
                if(_children.length < 2) {return;}
                var originFirstElement = _children[0].element;
                _children.reverse();
                if(_children[0].element !== originFirstElement)
                {_children[0].element.insertBefore(originFirstElement)}
                for(var i = 1; i < _children.length; ++i){
                    _children[i].element.insertAfter(_children[i-1].element);
                }
                this.element.trigger('change');
                this._notifier(this.getValue());
            },

            destroy: function (shouldDestroyElem) {
                for(var i = _children.length - 1; i >= 0; --i){
                    if(_children[i].hasOwnProperty('destroy')){
                        _children[i].destroy(shouldDestroyElem);
                        _children.pop();
                    }
                }
                if(shouldDestroyElem) {$(this.element).remove();}
                if(this.super != null && this.super.hasOwnProperty('destroy')){
                    this.super.destroy();
                }
                for (var key in this) {
                    try {delete this[key];} catch (e){ continue; }
                }
            },

            getRawValue: function () {
                var rawList = [];
                for(var i = 0; i < this.items.length; ++i){
                    rawList.push(this.items[i].getRawValue());
                }
                return rawList;
            },

            getValue: function () {
                var rawList = [];
                for(var i = 0; i < this.items.length; ++i){
                    rawList.push(this.items[i].getValue());
                }
                return rawList;
            },

            valueOf: function () {
                return this.getValue();
            },

            toString: function(){
                return JSON.stringify(this.getValue());
            },

            toRawString: function(){
                return JSON.stringify(this.getRawValue());
            },
        };
        var _node = this.super().extends(self);
        if (childrenModel != null) {_node.childModel = childrenModel;}
        return _node;
    }, {
        render: {
            default: function () {
                if(_typeof(this['element']) === 'string'){this.element = $(this.element);}
                this.element.html('');
                for(var i = 0; i < this.items.length; ++i){
                    this.element.append(this.items[i].element);
                }
                this.renderAttr();
                return this.element;
            },
            tbody: function () { return DataListNode.render.default.call(this, 'tbody'); },
            ul: function () { return DataListNode.render.default.call(this, 'ul'); },
            ol: function () { return DataListNode.render.default.call(this, 'ol'); },
            select: function () { return DataListNode.render.default.call(this, 'select'); }
        }
    });

    //支持数组定义和单个字符串定义
    fastDBDefinition = function (def, template, exception) {
        var result = {};
        for(var i = 0; i < def.length; ++i){
            result[def[i]] = {};
            if(template != null){ $.extend(true, result[def[i]], template); }
            if(exception != null && exception.hasOwnProperty(def[i]))
            {$.extend(true, result[def[i]], exception[def[i]]);}
            result[def[i]]['_name'] = def[i];
        }
        return result;
    };

    /** 生成有向图结构的数据库对象
     * @param {object|Array|string} definition 数据模型定义
     * @param {object} template 模板定义（仅当数据模型为Array/String时有效）
     * @param {object} exception 例外定义（覆盖模板定义）
     * @return {object} 数据库对象
     */
    Database = function (definition, template, exception) {
        var _db = {};
        var defineEntity = function (db, _name, entity) {
            Object.defineProperty(db, _name, {
                get: function () { return entity; },
                set: function (value) { entity.setValue(value); },
                enumerable: true,
                configurable: true
            });
        };

        var defType = _typeof(definition)
        if(defType === 'string'){definition = fastDBDefinition([definition], template, exception);}
        else if(defType === 'array'){ definition = fastDBDefinition(definition, template, exception)}

        if(definition.hasOwnProperty('child') && definition.hasOwnProperty('children')){
            FOError(0x023);
        }

        if(definition.hasOwnProperty('child')){
            var node = DataNode();
            for(var property in definition){
                if(!definition.hasOwnProperty(property)) {continue;}
                if(property !== 'child'){
                    Object.defineProperty(node, property, Object.getOwnPropertyDescriptor(definition, property));
                }
            }
            try{var nodeChild = Database(definition.child);}
            catch (stackoverflow) {
                if(stackoverflow instanceof RangeError){FOConsole.error(0x025); console.error(definition.child); FOError(0x026);}
                else {throw stackoverflow;}
            }

            if(nodeChild.type === 'DataCell' && nodeChild._name == null) {FOError(0x024)}
            node.setValue(nodeChild);
            if(definition.hasOwnProperty('_name')){
                defineEntity(_db, definition._name, node);
            }
            else{ _db = node; }
        }
        else if (definition.hasOwnProperty('children')){
            var list = DataListNode();
            for(var prop in definition){
                if(!definition.hasOwnProperty(prop)) {continue;}
                if(prop !== 'children'){
                    Object.defineProperty(list, prop, Object.getOwnPropertyDescriptor(definition, prop));
                }
            }
            list.childModel = definition.children;
            if(definition.hasOwnProperty('_name')){
                defineEntity(_db, definition._name, list);
            }
            else{ _db = list; }
        }
        else {
            var keywords = ['raw', 'render', 'filter', 'formatter', 'notifier', 'element', '_name', 'getElemValue'];
            var isDataCell = false;
            for(var identifier in definition){
                if(keywords.indexOf(identifier) >= 0) {
                    isDataCell = true;
                    break;
                }
            }

            if(isDataCell){
                var cell = DataCell(_typeof(definition['raw']));
                cell['_name'] = definition._name;
                for (var key in definition){
                    if(!definition.hasOwnProperty(key)) {continue;}
                    if(key !== 'raw') {
                        Object.defineProperty(cell, key, Object.getOwnPropertyDescriptor(definition, key));
                    }
                }
                cell.setValue(definition['raw']);

                if(definition.hasOwnProperty('_name')){
                    defineEntity(_db, definition._name, cell);
                }
                else{_db = cell;}
            }
            else{//Data Forest
                for(var fieldName in definition){
                    if(!definition.hasOwnProperty(fieldName)) { continue; }
                    if(!definition[fieldName].hasOwnProperty('_name')){definition[fieldName]._name = fieldName;}
                    try{FOAssign(_db, Database(definition[fieldName]));}
                    catch (stackoverflow) {
                        if(stackoverflow instanceof RangeError){FOConsole.error(0x025); console.error(definition[fieldName]); FOError(0x026);}
                        else {throw stackoverflow;}
                    }
                }
            }
        }

        return _db;
    };

    /** DataListNode 快捷生成方法（主要用于<table>）
     * @param {Array} childList DataCell名称表
     * @param {object} childDef DataCell模板定义（仅当数据模型为Array/String时有效）
     * @param {object} exceptionDef DataCell例外定义（覆盖模板定义）
     * @param {object} NodeDef DataListNode定义
     */
    Tablebase = function (childList, childDef, exceptionDef, NodeDef) {
        if(NodeDef == null) { NodeDef = {element: $('<tbody>')}; }
        if(!NodeDef.hasOwnProperty('children')) {NodeDef['children'] = {}}
        NodeDef.children['child'] = fastDBDefinition(childList, childDef, exceptionDef);
        if(!NodeDef.hasOwnProperty('render')) {NodeDef['render'] = DataListNode.render.tbody}
        return Database(NodeDef);
    }

    /** DataNode 快捷生成方法（主要用于<Form>）
     * @param {Array} childList DataCell名称表
     * @param {object} childDef DataCell模板定义（仅当数据模型为Array/String时有效）
     * @param {object} exceptionDef DataCell例外定义（覆盖模板定义）
     * @param {object} NodeDef DataListNode定义
     */
    Formbase = function (childList, childDef, exceptionDef, NodeDef) {
        if(NodeDef == null) { NodeDef = {element: $('<form>')}; }
        NodeDef['child'] = fastDBDefinition(childList, childDef, exceptionDef);
        var node = Database(NodeDef);
        $(node.element).on('reset', function () { node.reset(); });
        return node;
    }
    /**END_REGION(DATABASE)*/

    function _DeepFreeze(obj) {
        var propNames = Object.getOwnPropertyNames(obj);
        propNames.forEach(function(name) {
            var prop = obj[name];
            if (typeof prop == 'object' && prop !== null)
                _DeepFreeze(prop);
        });
        return Object.freeze(obj);
    }

    protocol = function(name, definition){
        var protocolTemplate = {
            name: name,
            type: 'ProtocolTemplate',
            content: {}, //definition goes here
            placeholder: {
                type: {}
            }, //virtual caller goes here. optional is meaningless here
        };
        for(var key in definition){
            if(!definition.hasOwnProperty(key)) {continue}
            if(_preserve.indexOf(key) > 0) {
                FOError(0x030, key);
            }
            if(!(definition[key] instanceof Array)){
                FOError(0x031);
            }

            //check definition
            var checker = {'var': 0, 'func': 0, 'get': 0, 'set': 0, 'static': 0, 'instance': 0, 'optional': 0};
            definition[key].forEach(function(descriptor){
                if(checker.hasOwnProperty(descriptor)) {checker[descriptor] += 1;}
                else {
                    FOError(0x032, descriptor, key);
                }
                if(checker[descriptor] > 1){
                    FOError(0x033, key);
                }
            });
            if (checker['var'] + checker['func'] > 1){
                FOError(0x034, key);
            }
            if (checker['var'] > 0 && checker['get'] + checker['set'] === 0){
                FOError(0x035);
            }
            if(checker['func'] > 0 && checker['get'] + checker['set'] > 0){
                FOError(0x036);
            }

            //generate placeholder
            var objDescriptor = {configurable:false, enumerable:true};
            if (checker['var'] > 0){
                if (checker['get'] > 0){ objDescriptor['get'] = function(){return null;}}
                if (checker['set'] > 0){ objDescriptor['set'] = function(newValue){}}
            }
            else if (checker['func'] > 0){
                objDescriptor['value'] = function(){ return null; };
            }
            if (checker['static'] > 0){
                Object.defineProperty(protocolTemplate.placeholder.type, key, objDescriptor);
                if (checker['instance'] > 0){
                    Object.defineProperty(protocolTemplate.placeholder, key, objDescriptor);
                }
            }
            else{
                Object.defineProperty(protocolTemplate.placeholder, key, objDescriptor);
            }
            protocolTemplate.content[key] = checker;
        }
        _DeepFreeze(protocolTemplate);

        Object.defineProperty(_global, name, {
            value: protocolTemplate,
            configurable: false,
            enumerable: true,
            writable: false,
        });
        return protocolTemplate;
    }

    protocol['entrust'] = function (aProtocol, required){
        if (aProtocol.type !== 'ProtocolTemplate') {
            FOError(0x037);
        }
        var protocolInstance = {
            get type()  {return 'ProtocolInstance'},
            get template() {return aProtocol},
            get required() {return required},
            delegator: null,
            virtualCaller: {
                get protocol() {return protocolInstance}
            }
        };
        for (var key in protocolInstance.template.content){
            Object.defineProperty(protocolInstance.virtualCaller, key, {configurable: false, enumerable: true,
                get: function(name){
                    return function(){
                        if(protocolInstance.delegator != null) {
                            if(protocolInstance.delegator.hasOwnProperty(name)){
                                if (typeof protocolInstance.delegator[name] === 'function')
                                    return function(){return protocolInstance.delegator[name].apply(protocolInstance.delegator, arguments)}
                                return protocolInstance.delegator[name]
                            }
                            else if (protocolInstance.template.content[name]['optional'] > 0 || !protocolInstance.required)
                                return protocolInstance.template.placeholder[name]
                            else
                                FOError(0x038, protocolInstance.delegator.type.className, protocolInstance.template.name);
                        }
                        else return protocolInstance.template.placeholder[name]
                    }
                }(key),
                set: function (name) {
                    return function(newValue){
                        if(protocolInstance.delegator != null) {
                            if(protocolInstance.delegator.hasOwnProperty(name))
                                protocolInstance.delegator[name] = newValue
                            else if (protocolInstance.template.content[name]['optional'] > 0 || !protocolInstance.required)
                                protocolInstance.template.placeholder[name] = newValue
                            else
                                FOError(0x038, protocolInstance.delegator.type.className, protocolInstance.template.name);
                        }
                        else return protocolInstance.template.placeholder[name] = newValue
                    }
                }(key)
            })
        }
        return protocolInstance.virtualCaller;
    }

    protocol['publish'] = function (protocolInstance, enstruster){
        if (!protocolInstance.hasOwnProperty('protocol') || protocolInstance.protocol.type !== 'ProtocolInstance') {
            FOError(0x039);
        }
        if (enstruster.hasOwnProperty('protocol') && enstruster.protocol instanceof Array){
            if(enstruster.protocol.indexOf(protocolInstance.protocol) < 0)
                enstruster.protocol.push(protocolInstance.protocol);
        }
        else{
            enstruster['protocol'] = [protocolInstance.protocol];
        }
    }

    function _isConform(aProtocol, delegator){
        for (var property in aProtocol.content){
            var checker = aProtocol.content[property];
            if (checker['optional'] > 0) {continue;}

            var throwErr = function () { FOError(0x03a, delegator.type.className, aProtocol.name, property) }

            var objDescriptor;
            if(checker['static'] > 0)
            { objDescriptor = Object.getOwnPropertyDescriptor(delegator.type, property); }
            else {objDescriptor = Object.getOwnPropertyDescriptor(delegator, property);}
            var insDescriptor = checker['instance'] > 0 ? Object.getOwnPropertyDescriptor(delegator, property) : undefined;
            if (objDescriptor != undefined){
                if(checker['var'] > 0){
                    if(checker['get'] > 0 && objDescriptor.get == undefined) {throwErr()}
                    if(checker['set'] > 0 && objDescriptor.set == undefined) {throwErr()}
                    if(checker['instance'] > 0) {
                        if(checker['get'] > 0 && insDescriptor.get == undefined) {throwErr()}
                        if(checker['set'] > 0 && insDescriptor.set == undefined) {throwErr()}
                    }
                }
                else{
                    if(typeof objDescriptor.value !== 'function') {throwErr()}
                    if(checker['instance'] > 0){
                        if(typeof insDescriptor.value !== 'function') {throwErr()}
                    }
                }
            }
            else{
                throwErr()
            }
        }
        return true;
    }

    protocol['delegate'] = function(aProtocol, delegator, entruster){
        if(_isConform(aProtocol, delegator)){
            if(!(entruster.protocol instanceof Array))
            {FOError(0x03b, entruster.type.className);}
            var hasEntrust = false;
            for (var i = 0; i < entruster.protocol.length; ++i){
                if(entruster.protocol[i].template === aProtocol){
                    hasEntrust = true;
                    entruster.protocol[i].delegator = delegator;
                    break
                }
            }
            if(!hasEntrust) {FOError(0x03c, entruster.type.className, aProtocol.name);}
        }
    }

    /** 检查代理方是否遵循某协议
     * @param {object} aProtocol 代理方实例
     * @param {object} delegator 协议模板，即 protocol() 的返回值
     */
    protocol['isConform'] = function(aProtocol, delegator){
        try {return _isConform(aProtocol, delegator);}
        catch (e) {return false;}
    }
}).call(this);
