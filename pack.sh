file="dist/UnTest.min.js"
uglifyjs UnTest.js -c -m --comments /@version/ -o $file
gsed -i ':a $!N; s/\n/ /; ta' $file
gsed -i s/"  \* @file "// $file
gsed -i s/" \* @author "/by/ $file
gsed -i s/" \* @version "/v/ $file
gsed -i s:"  \*/ ":"\*/": $file

ver=`gsed -n s/" \* @version "//p UnTest.js`
gsed -i s/.*version.*,/"  \"version\":\"$ver\","/ package.json