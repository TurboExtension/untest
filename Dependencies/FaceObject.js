﻿/**
 * @file MVC(MVVM)架构的JS基础类框架, 依赖于jquery
 * @author 孙嘉浩
 * @version 1.5.8
 */
if (this.hasOwnProperty('Class')) {
    this.alert('检测到非法篡改，建议立即更换安全的浏览器/网络/设备再访问本站点。');
    throw new Error('0x001');
}

var trigger, convert, protocol, FOAssign, _typeof, Database, FOError;
(function () {
    'use strict';
    var _preserve = ['type', 'typeName', 'isInstanceOf', 'protocol', 'super'];
    var _global = this;

    var _class2type = {};
    'Boolean Number String Function Array Date RegExp Object Error'.split(' ').forEach(function(e){
        _class2type[ '[object ' + e + ']' ] = e.toLowerCase();
    });
    _typeof = function(obj){
        if ( obj == null ){
            return String( obj );
        }
        return typeof obj === 'object' || typeof obj === 'function' ?
            _class2type[ _class2type.toString.call(obj) ] || 'object' :
            typeof obj;
    };

    FOError = function (errCode) {
        //in case hex number coverted to decimal by uglify-js
        var errMsg = '[FOError 0x' + errCode.toString(16) + ']';
        for(var i = 1; i < arguments.length; ++i){
            errMsg += ',' + arguments[i];
        }
        throw new Error(errMsg);
    }

    /** 定义一个类型
     *  @param {string} name 类型名
     *  @param {object} superClass 父类
     *  @param {function} initializer 初始化函数，返回类实例
     *  @param {object} [staticObjects] 静态属性
     */
    _global.Class = function(name, superClass, initializer, staticObjects){
        if (_global.hasOwnProperty(name)){ //不可被占用
            FOError(0x010, name);
        }
        if(superClass != null && (typeof superClass !== 'function' || !superClass.hasOwnProperty('super'))){
            FOError(0x011);
        }
        if(initializer == null || typeof initializer !== 'function'){
            FOError(0x012);
        }
        if(typeof staticObjects !== 'object' && staticObjects != null ) {
            FOError(0x013);
        }

        Object.defineProperty(_global, name, {
            value: function(){
                var instance = initializer.apply(_global[name], arguments); //修正this指向
                Object.defineProperty(instance, 'typeName', {value: name, configurable:false, enumerable:true, writable:false});
                Object.defineProperty(instance, 'type', {value: _global[name], configurable:false, enumerable:true, writable:false});
                Object.defineProperty(instance, 'isInstanceOf',{ value: function (classObj) {
                    if(this.type === classObj){return true;}
                    else if(this.hasOwnProperty('super')) { return this.super.isInstanceOf(classObj); }
                    return false;
                },configurable:false, enumerable:true, writable:false});
                return instance;
            },
            configurable: false,
            enumerable: true,
            writable: false
        });

        Object.defineProperty(_global[name], 'className', {
            value: name,
            configurable: false,
            enumerable: true,
            writable: false,
        });

        Object.defineProperty(_global[name], 'super', {
            value: superClass,
            configurable: false,
            enumerable: true,
            writable: false,
        });

        Object.defineProperty(_global[name], 'isSubClassOf', {
            value: function(classObj){
                if(!this.hasOwnProperty('super') || this['super'] == null || classObj == null)
                {return false;}
                else if (this.super === classObj){
                    return true;
                }
                else if (this.super.hasOwnProperty('isSubClassOf')){
                    return this.super.isSubClassOf(classObj);
                }
                else { return false; }
            },
            configurable: false,
            enumerable: true,
            writable: false
        });

        for (var key in staticObjects){
            if (!staticObjects.hasOwnProperty(key)) {return;}
            _global[name][key] = staticObjects[key];
        }
        return _global[name];
    };
    Object.defineProperty(_global, 'Class', {configurable: false, enumerable: true, writable: false});

    /** MVC JS 基类*/
    Class('JSObject', undefined, function()  {
        /** @private 私有成员区域 */
        //Note: this 指向类型
        //Note: 私有成员函数通过self访问实例

        //Note: 访问父类：this.super

        /** @public 公有成员区域 */
        var self = { //Note: this 指向实例
            //Note: super是undefined时，访问super理应出错。

            /** 销毁对象实例，使其无法再被使用。*/
            destroy: function () { //Note:成员函数内访问实例的super方法：this.super.method();
                for (var key in this) { //不会递归删除，因为类内保存的往往是对象的引用，一般不应因本实例销毁而销毁
                    try {delete this[key];} catch (e){ continue; }
                }
            },

            /** (@instance) 继承的实现方法
             * @param {string} subClass 子类的self对象
             */
            extends: function (subClass) {
                //实例super机制
                var instance_super = this;
                Object.defineProperty(subClass, 'super', {
                    get: function () { return instance_super; },
                    enumerable: true
                });
                //继承过程
                var instance = this.copy();
                for (var tkey in this) {
                    if (!subClass.hasOwnProperty(tkey) && _preserve.indexOf(tkey) < 0) {//仅保留被子类重写的部分
                        delete this[tkey];
                    }
                }
                FOAssign(instance, subClass);
                if (this['protocol'] instanceof Array && subClass['protocol'] instanceof Array){
                    this.protocol.forEach(function(p){
                        if (instance.protocol.indexOf(p) < 0) instance.protocol.push(p);
                    });
                }
                return instance;
            },

            /** 获得该对象的一份浅拷贝。*/
            copy: function () {
                var aCopy = {};
                for (var key in this) {
                    if(_preserve.indexOf(key) >= 0)
                        aCopy[key] = this[key];
                    else
                        Object.defineProperty(aCopy, key, Object.getOwnPropertyDescriptor(this, key));
                }
                return aCopy;
            }
        };

        /** @constructs 构造函数体区域 */

        return self;
    });


    /** 视图控制器类，拥有一个html元素作为主视图的控制器。
     * @param {string} viewSelector 元素选择器
     */
    Class('ViewController', JSObject, function (viewSelector) {

        /** @public */
        var self = {
            view: undefined,

            /** 显示主视图
             * @param {boolean} [animated] 指定是否使用$.fadeIn()动画
             */
            show: function (animated) {
                if (this.view === undefined) {
                    FOError(0x050);
                }
                if (animated !== undefined && animated) {
                    this.view.fadeIn(); //this 用于调用实例方法。
                }
                else {
                    this.view.show();
                }

                /** 为支持重写，实例调用父类函数时必须使用call指定this指针，使其指向实例。
                 * @example _super.show.call(this, animated)
                 */
            },

            /** 隐藏主视图
             * @param {boolean} [animated] 指定是否使用$.fadeOut()动画
             */
            hide: function (animated) {
                if (this.view === undefined) {
                    FOError(0x050);
                }
                if (animated !== undefined && animated) {
                    this.view.fadeOut();
                }
                else {
                    this.view.hide();
                }
            },

            /** @interface 预设视图 */
            preset: function () {
                FOError(0x014);
            }
        };

        /** @constructs */
        var _ViewController = this.super().extends(self);
        _ViewController.view = $(viewSelector);
        return _ViewController;
    });

    /**该方法对于传入的，用于绑定jquery事件的，实例方法进行转换，
     * 确保实例方法中的this指针始终指向实例，
     * 并将触发事件的元素以jquery object的形式作为参数传入。
     * @param {object} instance 实例指针
     * @param {function(jqObject)} handler 用于绑定jquery事件的实例方法。
     * @return {function} 转换后的方法，应该用以代替实例方法与jquery事件绑定
     */
    trigger = function (instance, handler) {
        var _converted = function (event) {
            handler.call(instance, $(this), event);
        };
        _converted['target'] = handler;
        return _converted;
    };

    /** 数据接口类，负责数据传输。
     *  @param {(object|object[])} [dataObj] 指定由接口管理的数据对象
     */
    Class('DataApi', JSObject, function (dataObj) {

        /** @public */
        var self = {
            data: undefined,

            /**
             * @callback responds 发起请求的阶段性回调方法，整个请求过程可能多次调用。
             * @param {boolean} success 请求成功标识符
             * @param {string} msg 请求状态描述信息
             */

            /** @interface 下载数据
             *  @param {function (boolean, string)} responds 发起请求的阶段性回调方法，整个请求过程可能多次调用。
             */
            download: function (responds) {
                FOError(0x014);
            },

            /** @interface 上传数据
             *  @param {function (boolean, string)} responds
             */
            upload: function (responds) {
                FOError(0x014);
            }
        };

        /** @constructs */
        var _DataApi = this.super().extends(self);
        _DataApi.data = dataObj;
        return _DataApi;
    });

    /** 该函数用来创建一个值类型转换方法
     * @param {string} type 类型描述
     * @param {any[]} [options] 当类型为option时，必须传入的选项值数组
     */
    convert = function(type, options) {
        switch (type) {
        case 'string':
            return function (value) { return value == null ? '' : value.toString(); };
        case 'int':
            return function (value) { return parseInt(value); };
        case 'float':
            return function (value) { return parseFloat(value); };
        case 'date':
            return function (value) { return new Date(value); };
        case 'option':
            return function (value) { return options[value]; };
        default:
            return function (value) { return value; };
        }
    };

    //polyfill for getOwnPropertyDescriptors
    if(!Object.hasOwnProperty('getOwnPropertyDescriptors')){
        Object.defineProperty(Object, 'getOwnPropertyDescriptors', {
            writable: true,
            configurable: true,
            value: function (obj) {
                var descriptors = Object.keys(obj).reduce(function(descriptors, key) {
                    descriptors[key] = Object.getOwnPropertyDescriptor(obj, key);
                    return descriptors;
                }, {});
                return descriptors;
            }
        });
    }

    /**将源对象的全部属性拷贝到目标对象中(浅拷贝)
     * @param {object} target 目标对象
     * @param {object} source 源对象
     * @return {object} 目标对象
     */
    FOAssign = function (target, source) {
        Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
        return target;
    };

    /**REGION(DATABASE)*/
    var _cellProto = {
        'boolean': Boolean,
        'number': Number,
        'string': String,
        'array':Array,
        'date':Date,
        // 'regExp': RegExp,
        // 'object': Object,
        // 'error': Error,
        // 'function':Function,
    };
    /** @Warning cut the code below before prepack, and paste it back in .pack.js and here*/
    (function(){
        for(var t in _cellProto){
            var proto = _cellProto[t].prototype;
            var pnames = Object.getOwnPropertyNames(proto);
            _cellProto[t] = {};
            for(var p = 0; p < pnames.length; ++p){
                Object.defineProperty(_cellProto[t], pnames[p], {get: function(pname){return function(){return this.raw[pname]}}(pnames[p]) })
            }
        }
    }).call();
    /** @Warning cut the code above before prepack, and paste it back in .pack.js and here*/

    Class('DataCell', JSObject, function(initType){
        var _raw = undefined;
        var _default = undefined;
        var self = {
            get raw(){
                return _raw;
            },
            reset: function () {
                this.setValue(_default);
            },

            render: function () {
                var value = this.getValue();
                if(!this.hasOwnProperty('element')){
                    var prefix = this['prefix'];
                    if(prefix == null){
                        if(/[A-Z]/.test(this._name.substring(0,1))){
                            prefix = '#';
                        }
                        else{
                            prefix = '.';
                        }
                    }
                    this['element'] = $(prefix + this._name + (this.hasOwnProperty('suffix') ? this.suffix : ''));
                }
                if(_typeof(this.element) === 'string'){this.element = $(this.element);}
                if(this.element.length === 0) {return;}

                var inputTag = ['INPUT', 'SELECT', 'TEXTAREA'];
                var instance = this;
                this.element.each(function (i, el) {
                    if(el.tagName == null){return;} //不支持文本节点
                    if($(el).attr('type') === 'checkbox'){
                        $(el).prop('checked', instance.raw == true).trigger('change');
                    }
                    else if($(el).attr('type') === 'radio'){
                        if($(el).attr('_name') == instance._name && $(el).val() == value){
                            $(el).prop('checked', true).trigger('change');
                        }
                    }
                    else if(inputTag.indexOf(el.tagName.toUpperCase()) >= 0){
                        if($(el).val() != value)
                        {$(el).val(value).trigger('input').trigger('change');}
                    }
                    else{
                        if($(el).text() != value)
                        {$(el).text(value).trigger('DOMCharacterDataModified').trigger('change');}
                    }

                    var eventObj = $._data(el, 'events');
                    if(eventObj == null || !eventObj.hasOwnProperty('change')){
                        $(el).on('input change propertychange DOMCharacterDataModified', trigger(instance, instance.elementEvent));
                    }
                    else{
                        var handlerExist = false;
                        for(var h = 0; h < eventObj.change.length; ++h){
                            var handlerObj = eventObj.change[h].handler;
                            if(handlerObj.hasOwnProperty('target') && handlerObj.target === instance.elementEvent){
                                handlerExist = true;
                            }
                        }
                        if(!handlerExist){
                            $(el).on('input change propertychange DOMCharacterDataModified', trigger(instance, instance.elementEvent));
                        }
                    }
                });

                return this.element;
            },

            elementEvent: function (elem) {
                var inputTag = ['INPUT', 'SELECT', 'TEXTAREA'];
                var elemValue;
                if(elem.attr('type') === 'checkbox'){
                    elemValue = elem.prop('checked');
                    if(_typeof(elemValue) == 'string')
                    {elemValue = elemValue === 'checked' ? true : false;}
                }
                else if(elem.attr('type') === 'radio'){
                    if(elem.attr('_name') != this._name){return;}
                    if(elem.val() == this.getValue()
                        && elem.prop('checked') !== true
                        && elem.prop('checked') !== 'checked'){
                        return;
                    }
                    elemValue = elem.val();
                }
                else if(inputTag.indexOf(elem[0].tagName.toUpperCase())>=0) { elemValue = elem.val(); }
                else { elemValue = elem.text(); }

                if(this.getValue() != elemValue) { this.setValue(elemValue); }
            },

            setValue: function (value) {
                var shouldSetDefault = false;
                if(_raw === undefined && _default === undefined) {shouldSetDefault = true;}
                if (this.hasOwnProperty('filter')) {
                    _raw = this.filter(value);
                }
                else {
                    _raw = value;
                }
                if(shouldSetDefault) {_default = _raw;}

                if (this.hasOwnProperty('render')) {
                    this.render();
                }
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            getValue: function () {
                if (this.hasOwnProperty('formatter')) {
                    return this.formatter(_raw);
                }
                else {
                    return _raw;
                }
            },

            //alias of getValue
            valueOf: function () {
                return this.getValue();
            },

            toString: function () {
                return this.getValue().toString();
            }
        };
        var _Cell = this.super().extends(self);
        if(_cellProto.hasOwnProperty(initType)){
            _Cell = Object.create(_cellProto[initType], Object.getOwnPropertyDescriptors(_Cell));
        }
        return _Cell;
    });

    Class('DataNode', JSObject, function () {
        var self = {
            getValue: function () {
                return this;
            },
            setValue: function (value) {
                if(this.hasOwnProperty('filter')) {value = this.filter(value);}
                if(_typeof(value) !== 'object'){
                    FOError(0x020);
                }

                for(var key in value){
                    if(!value.hasOwnProperty(key) || ['setValue', 'notifier', 'render'].indexOf(key) >= 0){ continue; }

                    if(this.hasOwnProperty(key)){
                        this[key] = value[key];
                    }
                    else{
                        Object.defineProperty(this, key, Object.getOwnPropertyDescriptor(value, key));
                    }
                    if(this[key].hasOwnProperty('notifier')){
                        this[key].notifier = function(childNotifier, instance, childName){
                            return function (){
                                childNotifier.call(instance[childName]);
                                if(instance.hasOwnProperty('notifier')){instance['notifier']()};
                            };
                        }(this[key].notifier, this, key);
                    }
                }
                if (this.hasOwnProperty('render')) {
                    this.render().trigger('change');
                }

                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            reset: function () {
                for(var child in this){
                    if(_typeof(this[child]) !== 'object' || !this[child].hasOwnProperty('reset')){ continue; }
                    this[child].reset();
                }
            },

            render: function () {
                if(!this.hasOwnProperty('element') && this.hasOwnProperty('_name')){
                    var prefix = this['prefix'];
                    if(prefix == null){
                        if(/[A-Z]/.test(this._name.substring(0,1))){
                            prefix = '#';
                        }
                        else{
                            prefix = '.';
                        }
                    }
                    this['element'] = $(prefix + this._name + (this.hasOwnProperty('suffix') ? this.suffix : ''));
                }
                if(_typeof(this['element']) === 'string'){this.element = $(this.element);}
                if(this['element'] == null || this['element'].length === 0) {return;}

                if(this.forceRedraw !== false){this.element.html('');}
                for(var child in this){
                    if((!this.hasOwnProperty(child)) || (_typeof(this[child])!== 'object')){continue;}
                    if(this[child].hasOwnProperty('render'))
                    {
                        var childElem = this[child].render();
                        if(childElem.length > 0){this.element.append(childElem);}
                    }
                    else if(this[child].hasOwnProperty('element'))
                    {this.element.append(this[child].element);}
                }
                return this.element;
            }
        };
        return this.super().extends(self);
    });

    Class('DataListNode', JSObject, function (childrenModel) {
        var _children = [];
        var _childModel = {};
        var _presetMap = {
            tbody: ['<tbody>', '<tr>', '<td>'],
            ul: ['<ul>', '<li>'],
            ol: ['<ol>', '<li>'],
            select: ['<select>', '<option>'],
            select_group: ['<select>', '<optgroup>', '<option>']
        };
        var _applyPresetRender = function () {
            var elemPreset;
            for(var preset in DataListNode['render']){
                if(this.render === DataListNode['render'][preset]){
                    elemPreset = _presetMap[preset];
                }
            }
            if(elemPreset == null) {return;}

            if(!this.hasOwnProperty('element') && this.hasOwnProperty('_name')){
                var prefix = this['prefix'];
                if(prefix == null){
                    if(/[A-Z]/.test(this._name.substring(0,1))){
                        prefix = '#';
                    }
                    else{
                        prefix = '.';
                    }
                }
                this['element'] = $(prefix + this._name + (this.hasOwnProperty('suffix') ? this.suffix : ''));
            }
            if(this['element'] == null || this['element'].length === 0) {
                this.element = elemPreset[0];
            }

            var layerIterator = function(layer, childPointer){
                if(!childPointer.hasOwnProperty('element')){
                    childPointer.element = elemPreset[layer];
                }

                if(childPointer.hasOwnProperty('children')){
                    layerIterator(layer + 1, childPointer.children);
                }
                else if(childPointer.hasOwnProperty('child')){
                    for(var subLayer in childPointer.child){
                        layerIterator(layer + 1, childPointer.child[subLayer]);
                    }
                }
            };
            layerIterator(1, this.childModel);
        };

        var self = {
            get items() {return _children;},
            set items(newValue) { this.setValue(newValue);},

            get childModel() {return _childModel;},
            set childModel(newValue) {
                _childModel = newValue;
                if(_childModel.hasOwnProperty('notifier')){
                    _childModel.notifier = function(childNotifier, instance){
                        childNotifier();
                        if(instance.hasOwnProperty('notifier')) {instance['notifier'];}
                    }(_childModel.notifier, this)
                }
                _applyPresetRender.call(this);
            },

            render: function () { return DataListNode.render.default.call(this);},

            //filter: function(value, replace){_typeof(value) === 'array' && _typeof(replace) === 'boolean'}
            setValue: function (value) {
                if(this.hasOwnProperty('filter')){value = this.filter(value, true);}
                if(_typeof(value) !== 'array'){ FOError(0x021); }

                _children = [];
                for (var i = 0; i < value.length; ++i){
                    var child = Database(this.childModel);
                    if(child.hasOwnProperty('setValue')){
                        child.setValue(value[i]);
                    }
                    else if(Object.keys(child).length === 1 && child[Object.keys(child)[0]].hasOwnProperty('setValue')){
                        child.setValue(value[i]);
                    }
                    else{
                        FOError(0x022);
                    }
                    child.render();
                    _children.push(child);
                }

                if (this.hasOwnProperty('render')) { this.render().trigger('change'); }
                if (this.hasOwnProperty('notifier')) { this.notifier(this.getValue()); }
            },

            reset: function () {
                for(var i = 0; i < this.items.length; ++i){
                    if(this.items[i].hasOwnProperty('reset')) {this.items[i].reset();}
                }
            },

            pop: function () {
                var child = _children.pop();
                child.element.remove();
                this.element.trigger('change');
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            push: function (value) {
                if(this.hasOwnProperty('filter')){value = this.filter([value], false)[0];}
                var child = Database(this.childModel);
                if(child.hasOwnProperty('setValue')){
                    child.setValue(value);
                }
                else if(Object.keys(child).length === 1 && child[Object.keys(child)[0]].hasOwnProperty('setValue')){
                    child.setValue(value);
                }
                else{
                    FOError(0x022);
                }
                _children.push(child);
                child.render().insertAfter(_children[_children.length - 1].element);
                this.element.trigger('change');
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            shift: function () {
                var child = _children.shift();
                child.element.remove();
                this.element.trigger('change');
                this.notifier(this.getValue());
            },

            splice: function (start, deleteCount) {
                var deleted;
                if(arguments.length > 2){
                    var argvs = [start, deleteCount];
                    for(var i = 2; i < arguments.length; ++i){
                        var child = Database(this.childModel);
                        var value;
                        if(this.hasOwnProperty('filter')) {value = this.filter([arguments[i]], false)[0];}
                        else{value = arguments[i];}

                        if(child.hasOwnProperty('setValue')){
                            child.setValue(value);
                        }
                        else if(Object.keys(child).length === 1 && child[Object.keys(child)[0]].hasOwnProperty('setValue')){
                            child.setValue(value);
                        }
                        else{
                            FOError(0x022);
                        }
                        child.render().insertBefore(_children[start].element);
                        argvs.push(child);
                    }
                    deleted = _children.slice.apply(_children, argvs);
                }
                else{
                    deleted = _children.slice.apply(arguments);
                }
                for(var k = 0; k <deleted.length; ++k){
                    deleted[k].element.remove();
                }
                this.element.trigger('change');
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            unshift: function () {
                this.splice.apply(this, [0, 0].concat(arguments));
                this.element.trigger('change');
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            sort: function () {
                _children.sort.apply(_children, arguments);
                for(var i = _children.length - 1; i >= 1 ; --i){
                    _children[i].element.insertAfter(_children[i - 1].element);
                }
                this.element.trigger('change');
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            reverse: function () {
                _children.reverse();
                for(var i = _children.length - 1; i >= 1 ; --i){
                    _children[i].element.insertAfter(_children[i - 1].element);
                }
                this.element.trigger('change');
                if (this.hasOwnProperty('notifier')) {
                    this.notifier(this.getValue());
                }
            },

            getValue: function () {
                return this.items;
            },

            valueOf: function () {
                return this.items;
            },

            toString: function(){
                return this.items.toString();
            }
        };
        var _node = this.super().extends(self);
        if (childrenModel != null) {_node.childModel = childrenModel;}
        return _node;
    }, {
        render: {
            default: function () {
                if(_typeof(this['element']) === 'string'){this.element = $(this.element);}
                this.element.html('');
                for(var i = 0; i < this.items.length; ++i){
                    this.element.append(this.items[i].element);
                }
                return this.element;
            },
            tbody: function () { return DataListNode.render.default.call(this, 'tbody'); },
            ul: function () { return DataListNode.render.default.call(this, 'ul'); },
            ol: function () { return DataListNode.render.default.call(this, 'ol'); },
            select: function () { return DataListNode.render.default.call(this, 'select'); }
        }
    });

    /** 生成有向图结构的数据库对象
     * @param {object|Array|string} definition 数据模型定义
     * @param {object} template 模板定义（仅当数据模型为Array/String时有效）
     * @param {object} exception 例外定义（覆盖模板定义）
     * @return {object} 数据库对象
     */
    Database = function (definition, template, exception) {
        var _db = {};
        var defineEntity = function (db, _name, entity) {
            Object.defineProperty(db, _name, {
                get: function () { return entity; },
                set: function (value) { entity.setValue(value); },
                enumerable: true,
                configurable: true
            });
        };

        //支持数组定义和单个字符串定义
        var makeDefinition = function (def) {
            var result = {};
            for(var i = 0; i < def.length; ++i){
                result[def[i]] = {};
                if(template != null){ $.extend(true, result[def[i]], template); }
                if(exception != null && exception.hasOwnProperty(def[i]))
                {$.extend(true, result[def[i]], exception[def[i]]);}
                result[def[i]]['_name'] = def[i];
            }
            return result;
        };

        var defType = _typeof(definition)
        if(defType === 'string'){definition = makeDefinition([definition]);}
        else if(defType === 'array'){ definition = makeDefinition(definition)}

        if(definition.hasOwnProperty('child')){
            var node = DataNode();
            for(var property in definition){
                if(!definition.hasOwnProperty(property)) {continue;}
                if(property !== 'child'){
                    Object.defineProperty(node, property, Object.getOwnPropertyDescriptor(definition, property));
                }
            }
            node.setValue(Database(definition.child));
            if(definition.hasOwnProperty('_name')){
                defineEntity(_db, definition._name, node);
            }
            else{ _db = node; }
        }
        else if (definition.hasOwnProperty('children')){
            var list = DataListNode();
            for(var prop in definition){
                if(!definition.hasOwnProperty(prop)) {continue;}
                if(prop !== 'children'){
                    Object.defineProperty(list, prop, Object.getOwnPropertyDescriptor(definition, prop));
                }
            }
            list.childModel = definition.children;
            if(definition.hasOwnProperty('_name')){
                defineEntity(_db, definition._name, list);
            }
            else{ _db = list; }
        }
        else {
            var keywords = ['raw', 'render', 'filter', 'formatter', 'notifier', 'element', '_name'];
            var isDataCell = false;
            for(var identifier in definition){
                if(keywords.indexOf(identifier) >= 0) {
                    isDataCell = true;
                    break;
                }
            }

            if(isDataCell){
                var cell = DataCell(_typeof(definition['raw']));
                cell['_name'] = definition._name;
                for (var key in definition){
                    if(!definition.hasOwnProperty(key)) {continue;}
                    if(key !== 'raw') {
                        Object.defineProperty(cell, key, Object.getOwnPropertyDescriptor(definition, key));
                    }
                }
                cell.setValue(definition['raw']);

                if(definition.hasOwnProperty('_name')){
                    defineEntity(_db, definition._name, cell);
                }
                else{_db = cell;}
            }
            else{//Data Forest
                for(var fieldName in definition){
                    if(!definition.hasOwnProperty(fieldName)) { continue; }
                    if(!definition[fieldName].hasOwnProperty('_name')){definition[fieldName]._name = fieldName;}
                    FOAssign(_db, Database(definition[fieldName]));
                }
            }
        }

        return _db;
    };
    /**END_REGION(DATABASE)*/

    /**REGION(PROTOCOl)*/
    function _DeepFreeze(obj) {
        var propNames = Object.getOwnPropertyNames(obj);
        propNames.forEach(function(name) {
            var prop = obj[name];
            if (typeof prop == 'object' && prop !== null)
                _DeepFreeze(prop);
        });
        return Object.freeze(obj);
    }

    // simple yet powerful and enough for use.
    // protocol('standardProtocol', {
    //     instanceProperty: ['var', 'get', 'set'],
    //     instanceFunction: ['func'],
    //     optionalFunc: ['func', 'optional'],
    //     optionalReadOnlyVar: ['var', 'get', 'optional'],
    //     typeProperty: ['var', 'static', 'get', 'set'],
    //     typeFunc: ['func', 'static', 'optional'],
    //
    //         //if you do need both static and instance method, provide an "instance" descriptor together.
    //     typeInstanceFunction: ['func', 'static', 'instance', 'optional'],
    // });

    protocol = function(name, definition){
        var protocolTemplate = {
            name: name,
            type: 'ProtocolTemplate',
            content: {}, //definition goes here
            placeholder: {
                type: {}
            }, //virtual caller goes here. optional is meaningless here
        };
        for(var key in definition){
            if(!definition.hasOwnProperty(key)) {continue;}
            if(_preserve.indexOf(key) >= 0) {
                FOError(0x030, key);
            }
            if(!(definition[key] instanceof Array)){
                FOError(0x031);
            }

            //check definition
            var checker = {'var': 0, 'func': 0, 'get': 0, 'set': 0, 'static': 0, 'instance': 0, 'optional': 0};
            definition[key].forEach(function(descriptor){
                if(checker.hasOwnProperty(descriptor)) {checker[descriptor] += 1;}
                else {
                    FOError(0x032, descriptor, key);
                }
                if(checker[descriptor] > 1){
                    FOError(0x033, key);
                }
            });
            if (checker['var'] + checker['func'] > 1){
                FOError(0x034, key);
            }
            if (checker['var'] > 0 && checker['get'] + checker['set'] === 0){
                FOError(0x035);
            }
            if(checker['func'] > 0 && checker['get'] + checker['set'] > 0){
                FOError(0x036);
            }

            //generate placeholder
            var objDescriptor = {configurable:false, enumerable:true};
            if (checker['var'] > 0){
                if (checker['get'] > 0){ objDescriptor['get'] = function(){return null;};}
                if (checker['set'] > 0){ objDescriptor['set'] = function(newValue){};}
            }
            else if (checker['func'] > 0){
                objDescriptor['value'] = function(){ return null; };
            }
            if (checker['static'] > 0){
                Object.defineProperty(protocolTemplate.placeholder.type, key, objDescriptor);
                if (checker['instance'] > 0){
                    Object.defineProperty(protocolTemplate.placeholder, key, objDescriptor);
                }
            }
            else{
                Object.defineProperty(protocolTemplate.placeholder, key, objDescriptor);
            }
            protocolTemplate.content[key] = checker;
        }
        _DeepFreeze(protocolTemplate);

        Object.defineProperty(_global, name, {
            value: protocolTemplate,
            configurable: false,
            enumerable: true,
            writable: false
        });
        return protocolTemplate;
    };

    /** 表达成为协议委托方的意愿
     * @param {object} aProtocol 协议模板，即 protocol() 的返回值
     * @param {boolean} [required] 指定协议是否为委托方必需，若为true，则代理方不能被更换。
     */
    protocol['entrust'] = function (aProtocol, required){
        if (aProtocol.type !== 'ProtocolTemplate') {
            FOError(0x037);
        }
        var protocolInstance = {
            get type()  {return 'ProtocolInstance';},
            get template() {return aProtocol;},
            get required() {return required;},
            delegator: null,
            virtualCaller: {
                get protocol() {return protocolInstance;}
            }
        };
        for (var key in protocolInstance.template.content){
            Object.defineProperty(protocolInstance.virtualCaller, key, {configurable: false, enumerable: true,
                get: function(name){
                    return function(){
                        if(protocolInstance.delegator != null) {
                            if(protocolInstance.delegator.hasOwnProperty(name)){
                                if (typeof protocolInstance.delegator[name] === 'function')
                                    return function(){return protocolInstance.delegator[name].apply(protocolInstance.delegator, arguments);};
                                return protocolInstance.delegator[name];
                            }
                            else if (protocolInstance.template.content[name]['optional'] > 0 || !protocolInstance.required)
                                return protocolInstance.template.placeholder[name];
                            else
                                FOError(0x038, protocolInstance.delegator.type.className, protocolInstance.template.name);
                        }
                        else return protocolInstance.template.placeholder[name];
                    };
                }(key),
                set: function (name) {
                    return function(newValue){
                        if(protocolInstance.delegator != null) {
                            if(protocolInstance.delegator.hasOwnProperty(name))
                                protocolInstance.delegator[name] = newValue;
                            else if (protocolInstance.template.content[name]['optional'] > 0 || !protocolInstance.required)
                                protocolInstance.template.placeholder[name] = newValue;
                            else
                                FOError(0x038, protocolInstance.delegator.type.className, protocolInstance.template.name);
                        }
                        else return protocolInstance.template.placeholder[name] = newValue;
                    };
                }(key)
            });
        }
        return protocolInstance.virtualCaller;
    };

    /** 发布委托协议
     * @param {object} protocolInstance 协议实例，即 entrust() 的返回值
     * @param {object} enstruster 协议的实例
     */
    protocol['publish'] = function (protocolInstance, enstruster){
        if (!protocolInstance.hasOwnProperty('protocol') || protocolInstance.protocol.type !== 'ProtocolInstance') {
            FOError(0x039);
        }
        if (enstruster.hasOwnProperty('protocol') && enstruster.protocol instanceof Array){
            if(enstruster.protocol.indexOf(protocolInstance.protocol) < 0)
                enstruster.protocol.push(protocolInstance.protocol);
        }
        else{
            enstruster['protocol'] = [protocolInstance.protocol];
        }
    };

    function _isConform(aProtocol, delegator){
        for (var property in aProtocol.content){
            var checker = aProtocol.content[property];
            if (checker['optional'] > 0) {continue;}

            var throwErr = function () { FOError(0x03a, delegator.type.className, aProtocol.name, property) }

            var objDescriptor;
            if(checker['static'] > 0)
            { objDescriptor = Object.getOwnPropertyDescriptor(delegator.type, property); }
            else {objDescriptor = Object.getOwnPropertyDescriptor(delegator, property);}
            var insDescriptor = checker['instance'] > 0 ? Object.getOwnPropertyDescriptor(delegator, property) : undefined;
            if (objDescriptor != null){
                if(checker['var'] > 0){
                    if(checker['get'] > 0 && objDescriptor.get == null) {throwErr();}
                    if(checker['set'] > 0 && objDescriptor.set == null) {throwErr();}
                    if(checker['instance'] > 0) {
                        if(checker['get'] > 0 && insDescriptor.get == null) {throwErr();}
                        if(checker['set'] > 0 && insDescriptor.set == null) {throwErr();}
                    }
                }
                else{
                    if(typeof objDescriptor.value !== 'function') {throwErr();}
                    if(checker['instance'] > 0){
                        if(typeof insDescriptor.value !== 'function') {throwErr();}
                    }
                }
            }
            else{
                throwErr();
            }
        }
        return true;
    }

    /** 表达成为协议代理方的意愿
     * @param {object} aProtocol 协议模板，即 protocol() 的返回值
     * @param {object} delegator 代理方实例
     * @param {object} entruster 委托方实例
     */
    protocol['delegate'] = function(aProtocol, delegator, entruster){
        if(_isConform(aProtocol, delegator)){
            if(!(entruster.protocol instanceof Array))
            {FOError(0x03b, entruster.type.className);}
            var hasEntrust = false;
            for (var i = 0; i < entruster.protocol.length; ++i){
                if(entruster.protocol[i].template === aProtocol){
                    hasEntrust = true;
                    entruster.protocol[i].delegator = delegator;
                    break;
                }
            }
            if(!hasEntrust) {FOError(0x03c, entruster.type.className, aProtocol.name);}
        }
    };

    /** 检查代理方是否遵循某协议
     * @param {object} aProtocol 代理方实例
     * @param {object} delegator 协议模板，即 protocol() 的返回值
     */
    protocol['isConform'] = function(aProtocol, delegator){
        try {return _isConform(aProtocol, delegator);}
        catch (e) {return false;}
    };
    /**END_REGION(PROTOCOl)*/

    /** @tutorial 最佳实践
     *
     * - 继承自JSObject的类，应当确保实例的公有方法中的this指针，始终指向实例本身。
     *
     * - js毕竟是动态脚本语言，使用面向对象的方法会加重浏览器脚本解析引擎的负担，
     *   因此编写时需注意以下几点：
     *   1. 类型定义、实例化的开销不可忽视，尤其是跟不使用本框架时相比。
     *   2. 基于本框架编写的脚本，其脚本标记中最好加上defer关键字，
     *      例如： <script defer src="XXXController.js"></script>
     *      这是为了防止浏览器为了解析脚本导致页面加载缓慢。
     *      （引用本框架的标记除外，因为错误的脚本加载顺序将导致脚本无法执行）
     *
     * - 架构说明
     *   1. DataApi 用于处理数据传输问题，包括网路传输，和应用内数据访问； Controller 初始化view并管理 View 事件；
     *      View即浏览器渲染的Html元素实例。
     *      为实现 Model 和 Controller 间的交互，可以让 Controller 持有 Model 实例，
     *      但不能反过来让 Model 持有 Controller 实例。
     *   2. 本框架本质上是 MVVM 架构，若项目规模较小，直接作普通 MVC 架构用亦可。
     *      不同的页面规模不一，也可灵活使用不同架构。
     */
}).call(this);


